# VR Conversation Training

The Virtual Reality Conversation Training (VRCT) is a framework that allows you to practice standardized conversations in virtual scenarios. It consists of several components, which enable the user to create, execute and evaluate different training scenarios. An overview of the core functionality and possible applications can be found in the following publication: https://journals.sagepub.com/doi/full/10.1177/00938548221124128?af=R&ai=1gvoi&mi=3ricys
 
To set up the backend and the operator app, follow the instructions in the respective directories. 
 
We will continuously work on the wiki to describe setup and workflow extensively.
 
If you have any questions or need help, feel free to contact us:
 
hermannbarbe@googlemail.com

peter.fromberger@med.uni-goettingen.de
