# API - Backend for Virtual Conversation Training

## Installation for devs

1. Install virtual python environment in the api_backend folder

```bash
python -m venv env
```

2. Activate the env

```bash
env\Scripts\Activate.ps1
```

3. Install requirements within the virtual environment

```bash
pip install -r requirements.txt
```

### start vrct-backend

4. Initialize sqlite3 database

```bash
python manage.py makemigrations vrct_app
```

```bash
python manage.py migrate
```

5. Start dev server

```bash
python manage.py runserver
```
