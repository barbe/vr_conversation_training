from django.contrib import admin
from vrct_app.models import *

# ------
# Trainee
# ------
@admin.register(Trainee)
class TraineeAdmin(admin.ModelAdmin):
    '''Admin View for Trainee model'''

    list_display = ['trainee_id', 'trainee_training', 'trainee_group', 'trainee_is_pilot', 'created_at', 'updated_at', 'created_by']
    list_filter = ('trainee_training', 'trainee_group', 'trainee_is_pilot',)
    search_fields = ['trainee_id']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


# ------
# Training
# ------
@admin.register(Training)
class TrainingAdmin(admin.ModelAdmin):
    '''Admin View for Training model'''

    list_display = ['name', 'short_description', 'created_at', 'updated_at', 'created_by']
    list_filter = ('name',)
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

@admin.register(Bot)
class BotAdmin(admin.ModelAdmin):
    '''Admin View for Bot model'''

    list_display = ['name', 'vignette', 'created_at', 'updated_at', 'created_by']
    list_filter = ('name',)
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

@admin.register(BotVariant)
class BotvariantAdmin(admin.ModelAdmin):
    '''Admin View for Bot variant model'''

    list_display = ['name', 'short_description', 'bot', 'created_at', 'updated_at', 'created_by']
    list_filter = ('name',)
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

@admin.register(Condition)
class ConditionAdmin(admin.ModelAdmin):
    '''Admin View for Condition model'''

    list_display = ['name', 'get_trainings', 'bot', 'get_bot_variants', 'get_questionnaires_pre', 'get_questionnaires_post', 'unity_scene', 'has_feedback', 'feedback_table', 'created_at', 'updated_at', 'created_by']
    list_filter = ('name',)
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

@admin.register(ExperimentalGroup)
class ExperimentalGroupAdmin(admin.ModelAdmin):
    '''Admin View for Experimental Group model'''

    list_display = ['name', 'get_trainings', 'created_at', 'updated_at', 'created_by']
    list_filter = ('name',)
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

# ------
# Questionnaires
# ------
@admin.register(QListModel)
class QListModelAdmin(admin.ModelAdmin):
    '''Admin View for QListModel model'''

    list_display = ['name', 'created_at', 'updated_at', 'created_by']
    list_filter = ('name',)
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

# Add your own questionnaires beyond

@admin.register(QuestionnaireOne)
class QuestionnaireOneAdmin(admin.ModelAdmin):
    '''Admin View for QuestionnaireOne model'''

    list_display = [field.name for field in QuestionnaireOne._meta.get_fields()]

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

@admin.register(QuestionnaireTwo)
class QuestionnaireTwoAdmin(admin.ModelAdmin):
    '''Admin View for QuestionnaireOne model'''

    list_display = [field.name for field in QuestionnaireTwo._meta.get_fields()]

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

# ------
# Data Tables
# ------
@admin.register(DListModel)
class DListModelAdmin(admin.ModelAdmin):
    '''Admin View for DListModel model'''

    list_display = ['name', 'created_at', 'updated_at', 'created_by']
    list_filter = ('name',)
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


# data table for Tutorial Training
@admin.register(TutorialTrainingData)
class TutorialTrainingDataAdmin(admin.ModelAdmin):
    '''Admin View for TutorialTrainingData model'''

    list_display = [field.name for field in TutorialTrainingData._meta.get_fields()]

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


# ------
# Running Training
# ------
@admin.register(CurrentRunningTraining)
class CurrentRunningTrainingAdmin(admin.ModelAdmin):
    '''Admin View for CurrentRunningTraining model'''

    list_display = ['trainee', 'training', 'condition', 'bot', 'bot_variant', 'q_pre_finished', 'qs_pre', 'ws_room_name', 'training_running', 'training_finished', 'q_post_finished', 'qs_post']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


# ------
# Feedback
# ------
@admin.register(FListModel)
class FListModelAdmin(admin.ModelAdmin):
    '''Admin View for CurrentRunningTraining model'''

    list_display = ['name', 'created_at', 'updated_at', 'created_by']
    list_filter = ('name',)
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


# feedback table for Tutorial Training
@admin.register(TutorialTrainingFeedback)
class TutorialTrainingFeedbackAdmin(admin.ModelAdmin):
    '''Admin View for TutorialTrainingData model'''

    list_display = [field.name for field in TutorialTrainingFeedback._meta.get_fields()]

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

