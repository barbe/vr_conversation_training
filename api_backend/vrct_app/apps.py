from django.apps import AppConfig
from django.db.models.signals import post_migrate


class VrctAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vrct_app'

    def ready(self):
        from vrct_app.signals import populate_models
        post_migrate.connect(populate_models, sender=self)
