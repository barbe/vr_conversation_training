from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from datetime import datetime

class ChatConsumer(JsonWebsocketConsumer):
    '''
    Please see https://channels.readthedocs.io/en/stable/topics/consumers.html for detailed infos.
    
    '''
    def connect(self):

        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        print("New Connection to Websocket!")
        print(self.scope["user"])

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        # accept new connection
        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data=None, bytes_data=None, **kwargs):
        if text_data:
            try:
                self.receive_json(self.decode_json(text_data), **kwargs)
            except:
                print("text_data is not in json-format!")
                print("text_data: " + text_data)
        else:
            raise ValueError("No text section for incoming WebSocket frame!")

    def receive_json(self, content):

        # unity sends messages in the format {'message': [{...}]}
        # we have to standardize the message from unity
        try:
            # if the message comes not from unity
            message = content['message']
        except:
            # if it is message from unity
            message = content

        # add timestamp
        message[0]["timestamp"] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat.message',
                'message': message
            }
        )

    # Receive message from room group
    # here goes the message from a client
    def chat_message(self, event):

        # filter out heartbeats
        if event["message"][0]["type"] == "heartbeat":
            pass
        else:
            print('consumer: Receive message from room group:')
            print(event["message"])
        
        # Send message to WebSocket
        self.send_json(event["message"])

        