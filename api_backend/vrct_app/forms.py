from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django.db.models import Q

from vrct_app.models import Trainee, Training, Condition, Bot, BotVariant, ExperimentalGroup

# -----
# CREATE NEW Trainee
# -----
class TraineeForm(forms.ModelForm):
    
    class Meta:
        model = Trainee
        fields = ("trainee_id", "trainee_training", "trainee_group", "trainee_is_pilot", )

    def clean_trainee_id(self):
        trainee_id = self.cleaned_data['trainee_id']

        if Trainee.objects.filter(trainee_id = trainee_id).exists():

            raise forms.ValidationError(u'The Trainee id "%s" already exists in the database. Please use a unique Trainee id.' % trainee_id)

        else:
            return trainee_id


# -----
# CREATE NEW TRAINING
# -----
class TrainingForm(forms.ModelForm):
    
    class Meta:
        model = Training
        fields = ("name", "short_description", "data_table")

    def clean_name(self):
        name = self.cleaned_data['name']

        if Training.objects.filter(name = name).exists():

            raise forms.ValidationError(u'The training name "%s" already exists in the database. Please use a unique name for your new training.' % name)

        else:
            return name


class ConditionForm(forms.ModelForm):
    
    class Meta:
        model = Condition
        fields = ("name", "trainings", 'bot', 'bot_variants', 'questionnaires_pre', 'questionnaires_post', 'unity_scene', 'has_feedback', 'feedback_table')

    def clean_name(self):
        name = self.cleaned_data['name']

        if Condition.objects.filter(name = name).exists():

            raise forms.ValidationError(u'The training condition name "%s" already exists in the database. Please use a unique name for your new training condition.' % name)

        else:
            return name


class GroupForm(forms.ModelForm):
    
    class Meta:
        model = ExperimentalGroup
        fields = ("name", "trainings")

    def clean_name(self):
        name = self.cleaned_data['name']

        if ExperimentalGroup.objects.filter(name = name).exists():

            raise forms.ValidationError(u'The experimental group name "%s" already exists in the database. Please use a unique name for your new experimental group.' % name)

        else:
            return name


class BotForm(forms.ModelForm):
    
    class Meta:
        model = Bot
        fields = ("name", "vignette")

    def clean_name(self):
        name = self.cleaned_data['name']

        if Bot.objects.filter(name = name).exists():

            raise forms.ValidationError(u'The bot name "%s" already exists in the database. Please use a unique name for your new bot.' % name)

        else:
            return name

class BotVariantForm(forms.ModelForm):
    
    class Meta:
        model = BotVariant
        fields = ("name", "short_description", "bot")


class UserForm(UserCreationForm):

    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.filter(Q(name='Administrator') | Q(name='Training Operator')))

    class Meta:
        model = User
        fields = ["first_name", "last_name", "username", "password1", "password2", "groups"]


# Run Training
class StartTrainingForm(forms.Form):

    training = forms.ModelChoiceField(queryset=Training.objects.all())
    trainee = forms.ChoiceField()


# dynamic form for questionnaires
def get_q_form_unbound(model_class):
    class QForm(forms.ModelForm):
        class Meta:
            model = model_class
            exclude = ('trainee', 'training', 'condition', 'timepoint', 'created_by',)

    return QForm()

def get_q_form_bound(model_class, request):
    class QForm(forms.ModelForm):
        class Meta:
            model = model_class
            exclude = ('trainee', 'training', 'condition', 'timepoint', 'created_by',)

    return QForm(request.POST)


# Start Export
class StartExportForm(forms.Form):

    training = forms.ModelChoiceField(queryset=Training.objects.all())

