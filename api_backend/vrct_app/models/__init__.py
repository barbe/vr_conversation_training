# import all models
from vrct_app.models.trainings import *
from vrct_app.models.questionnaires import *
from vrct_app.models.data import *
from vrct_app.models.feedback import *