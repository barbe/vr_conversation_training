# Common infos for all models
from django.db import models
from django.utils.timezone import now
from django.conf import settings


class VrctBaseModel(models.Model):
    """ A abstract model used in each model within vrct."""
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField(editable=False)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        abstract = True

    def save(self, **kwargs):
        if not self.created_at:
            self.created_at = now()
        self.updated_at = now()
        super(VrctBaseModel, self).save()