from django.db import models
from vrct_app.models.base import VrctBaseModel
from vrct_app.models.trainings import Trainee, Training, Condition, Bot, BotVariant


class DListModel(VrctBaseModel):
    """Model definition for DList. The name of each data table must be unique.
    Here we list all data table models defined beyond to be able to choose them while
    creation of a training."""

    name = models.CharField(verbose_name="name", max_length=255, unique = True)

    class Meta:
        """Meta definition for DListModel."""
        verbose_name = 'DListModel'
        verbose_name_plural = 'DListModels'
        app_label = 'vrct_app'

    def __str__(self):
        return self.name


# -----------------
# Make your own data tables for your trainings
# -----------------

class TutorialTrainingData(VrctBaseModel):
    """ Example of an training data model with bot conversation."""

    BEHAVIOR_CHOICES = [('communicative', 'communicative'), ('reserved', 'reserved')]

    TEST1_CHOICES = [(1, 'one'), (2, 'two'), (3, 'three')]

    # PLEASE INTEGRATE THIS IN EVERY TABLE FOR A NEW TRAINING!
    # basic info about trainee, training etc.
    trainee = models.ForeignKey(Trainee, on_delete=models.CASCADE, related_name="tutorial_data_trainee")
    training = models.ForeignKey(Training, on_delete=models.CASCADE, related_name="tutorial_data_training")
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE, related_name="tutorial_data_condition")
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name="tutorial_data_bot")
    bot_variant = models.ForeignKey(BotVariant, on_delete=models.CASCADE, related_name="tutorial_data_bot_variant")

    # PLEASE INTEGRATE THIS IN EVERY TABLE FOR A NEW TRAINING!
    # basic trial info (one trial is defined as a question-answer-pair)
    trial_nr = models.IntegerField()    # starts always with 1
    trial_start_time = models.DateTimeField()   # time at which the trial starts = trainee asks a question
    trial_end_time = models.DateTimeField() # time at which the trial end = chatscript sends answer to Unity
    trial_duration = models.FloatField()  # in ms = trial_end_time - trial_start_time

    # PLEASE INTEGRATE THIS IN EVERY TABLE FOR A NEW TRAINING!
    # basic question and answer content variables
    question_content = models.TextField()
    answer_content = models.TextField()

    # bot output variables
    # IMPORTANT: they always must start with 'cs_'
    cs_topic = models.CharField(max_length=255)
    cs_answerCategory = models.CharField(max_length=255)
    cs_actionType = models.CharField(max_length=255)
    cs_questionCounter = models.IntegerField()
    cs_soundFile = models.CharField(max_length=255)
    cs_probFormat = models.CharField(max_length=255)
    cs_randNum = models.IntegerField()

    # bot input variables
    # IMPORTANT: if the variable should/must be categorized by the operator, 
    # you MUST define the variable starting with 'operator_'
    # you also MUST define the verbose_name and a help_text!
    operator_behavior = models.CharField(max_length=255, choices=BEHAVIOR_CHOICES, verbose_name="Behavior", help_text="Please categorize the verbal behavior of the trainee.")

    class Meta:
        """Meta definition for Tutorial Training Data."""
        verbose_name = 'tutorial training data'
        verbose_name_plural = 'tutorial training data'
        app_label = 'vrct_app'
        unique_together = ['trainee', 'training', 'condition', 'bot', 'bot_variant', 'trial_nr']