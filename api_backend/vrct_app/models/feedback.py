from django.db import models
from vrct_app.models.data import TutorialTrainingData
from vrct_app.models.base import VrctBaseModel
from vrct_app.models.trainings import Trainee, Training, Condition, Bot, BotVariant


class FListModel(VrctBaseModel):
    """Model definition for FList. The name of each feedback table must be unique.
    Here we list all feedback models defined beyond to be able to choose them while
    creation of training condition."""

    name = models.CharField(verbose_name="name", max_length=255, unique = True)

    class Meta:
        """Meta definition for FListModel."""
        verbose_name = 'FListModel'
        verbose_name_plural = 'FListModels'
        app_label = 'vrct_app'

    def __str__(self):
        return self.name

# -----------------
# Make your own data tables for your feedbacks
# -----------------

class TutorialTrainingFeedback(VrctBaseModel):
    """Feedback Model for the tutorial training"""

    # basic info about trainee, training etc.
    trainee = models.ForeignKey(Trainee, on_delete=models.CASCADE, related_name="tutorial_feedback_trainee")
    training = models.ForeignKey(Training, on_delete=models.CASCADE, related_name="tutorial_feedback_training")
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE, related_name="tutorial_feedback_condition")
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name="tutorial_feedback_bot")
    bot_variant = models.ForeignKey(BotVariant, on_delete=models.CASCADE, related_name="tutorial_feedback_bot_variant")

    # adjustable fields
    n_communicative_behavior = models.IntegerField()
    n_reserved_behavior = models.IntegerField()

    good_trial_question_content = models.TextField()
    good_trial_answer_content = models.TextField()

    bad_trial_question_content = models.TextField()
    bad_trial_answer_content = models.TextField()

    class Meta:
        """Meta definition for Tutorial Training Feedback."""
        verbose_name = 'tutorial training feedback'
        verbose_name_plural = 'tutorial training feedbacks'
        app_label = 'vrct_app'
        unique_together = ['trainee', 'training', 'condition']