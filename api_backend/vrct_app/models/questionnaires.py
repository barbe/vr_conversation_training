from django.db import models
from vrct_app.models.base import VrctBaseModel
from vrct_app.models.trainings import Trainee, Training, Condition


# -------------
# Please define here your questionnaires
# -------------
class QuestionnaireOne(VrctBaseModel):
    """Model definition for QuestionnaireOne. This is an example for the tutorial."""

    TIMEPOINT_CHOICES = [('pre', 'pre'), ('post', 'post')]

    # basic info about trainee, training etc.
    trainee = models.ForeignKey(Trainee, on_delete=models.CASCADE, related_name="q1_trainee")
    training = models.ForeignKey(Training, on_delete=models.CASCADE, related_name="q1_training")
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE, related_name="q1_condition")
    timepoint = models.CharField(choices=TIMEPOINT_CHOICES, max_length=255)

    # Items of Questionnaire
    ITEM_ONE_CHOICES = [(1, 'yes'), (2, 'no')]
    ITEM_TWO_CHOICES = [('yes', 'yes'), ('no', 'no')]

    item1 = models.IntegerField(verbose_name="Was the training impressive?", choices=ITEM_ONE_CHOICES)
    item2 = models.CharField(max_length=255, verbose_name="Do you think the training is helpful?", choices=ITEM_TWO_CHOICES)

    class Meta:
        """Meta definition for QuestionnaireOne."""
        verbose_name = 'QuestionnaireOne'
        verbose_name_plural = 'QuestionnaireOnes'
        app_label = 'vrct_app'
        unique_together = ['trainee', 'training', 'condition', 'timepoint']

    def __str__(self):
        return "QuestionnaireOne"

class QuestionnaireTwo(VrctBaseModel):
    """Model definition for QuestionnaireTwo. This is an example for the tutorial."""

    TIMEPOINT_CHOICES = [('pre', 'pre'), ('post', 'post')]
    ITEM_ONE_CHOICES = [(1, 'yes'), (2, 'no')]
    ITEM_TWO_CHOICES = [('yes', 'yes'), ('no', 'no')]

    # basic info about trainee, training etc.
    trainee = models.ForeignKey(Trainee, on_delete=models.CASCADE, related_name="q2_trainee")
    training = models.ForeignKey(Training, on_delete=models.CASCADE, related_name="q2_training")
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE, related_name="q2_condition")
    timepoint = models.CharField(choices=TIMEPOINT_CHOICES, max_length=255)

    # Items of the questionnaire
    item1 = models.IntegerField(verbose_name="Will the training help you in future situations?", choices=ITEM_ONE_CHOICES)
    item2 = models.CharField(max_length=255, verbose_name="Did you have the feeling that your virtual conversation partner was real?", choices=ITEM_TWO_CHOICES)

    class Meta:
        """Meta definition for QuestionnaireTwo."""
        verbose_name = 'QuestionnaireTwo'
        verbose_name_plural = 'QuestionnaireTwos'
        app_label = 'vrct_app'
        unique_together = ['trainee', 'training', 'condition', 'timepoint']

    def __str__(self):
        return "QuestionnaireTwo"
