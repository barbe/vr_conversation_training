from django.db import models
from vrct_app.models.base import VrctBaseModel


class QListModel(VrctBaseModel):
    """Model definition for QList. The name of each questionnaire must be unique.
    Here we list all questionnaire models defined beyond to be able to choose them while
    creation of training condition."""

    name = models.CharField(verbose_name="name", max_length=255, unique = True)

    class Meta:
        """Meta definition for QListModel."""
        verbose_name = 'QListModel'
        verbose_name_plural = 'QListModels'
        app_label = 'vrct_app'

    def __str__(self):
        return self.name

class Training(VrctBaseModel):
    """Model definition for Training. The name of each training must be unique. 
    A training has one fixed data table in which dependent variables for each trials are defined. Thus, all associated
    conditions and bots (and bot variants) have to follow this data table."""

    name = models.CharField(verbose_name="name", max_length=255, unique = True)
    short_description = models.TextField(verbose_name="short description", null=True, blank=True)

    data_table = models.ForeignKey('vrct_app.DListModel', on_delete=models.CASCADE, related_name="data_table_name")

    class Meta:
        """Meta definition for Training."""
        verbose_name = 'training'
        verbose_name_plural = 'trainings'
        app_label = 'vrct_app'

    def __str__(self):
        return self.name


class ExperimentalGroup(VrctBaseModel):
    """Model definition for a ExperimentalGroup. The name of ExperimentalGroup must be unique."""

    name = models.CharField(verbose_name="name", max_length=255, unique = True)

    # many-to-many field: an Condition can be used in multiple Trainings, and a 
    # Training can have multiple Conditions
    trainings = models.ManyToManyField(Training)


    class Meta:
        """Meta definition for Experimental Groups."""
        verbose_name = 'experimental group'
        verbose_name_plural = 'experimental groups'
        app_label = 'vrct_app'

    def __str__(self):
        return self.name

    def get_trainings(self):
        return ", ".join([str(p) for p in self.trainings.all()])


class Bot(VrctBaseModel):
    """Model definition for a ChatScript bot. The name of each bot must be unique. 
    Each bot associated with one experiment MUST follow the same data table."""

    name = models.CharField(verbose_name="name", max_length=255, unique = True)
    vignette = models.TextField(verbose_name="vignette")

    class Meta:
        """Meta definition for Bot."""
        verbose_name = 'bot'
        verbose_name_plural = 'bots'
        app_label = 'vrct_app'

    def __str__(self):
        return self.name

    def get_variants(self):
        return ", ".join([str(p) for p in self.variants.all()])


class BotVariant(VrctBaseModel):
    """Model definition for a bot variant. A bot can have multiple variants. 
    Please see documentation for conversation engine to 
    learn more about bot variants."""

    name = models.CharField(verbose_name="name", max_length=255)
    short_description = models.TextField(verbose_name="short description", null=True, blank=True)
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name="bot_variant")

    class Meta:
        """Meta definition for Bot Variant."""
        verbose_name = 'botvariant'
        verbose_name_plural = 'botvariants'
        app_label = 'vrct_app'
        unique_together = ['name', 'bot']

    def __str__(self):
        return self.name



class Condition(VrctBaseModel):
    """Model definition for a Training condition (or measurement point). The name of each training condition must be unique.
    Each condition must follow the data table defined in its training."""

    name = models.CharField(verbose_name="name", max_length=255, unique = True)

    # each condition must be associated with at least one Training, but it can be associated with more...
    trainings = models.ManyToManyField(Training)

    # each condition must have at least one bot, but it can have more than one bot...
    # in the operator_application the operator can choose, which bot (and which bot variant) will be used.
    # if there is only one bot (with one variant) in a condition, only this bot can be used in the condition
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name = 'condition_bot')
    bot_variants = models.ManyToManyField(BotVariant)

    # questionnaires associated with this condition. You can define questionnaires showed before (pre)
    # and after (post) the condtion. Questionnaires are not required...
    questionnaires_pre = models.ManyToManyField(QListModel, related_name="qlist_pre", blank=True)
    questionnaires_post = models.ManyToManyField(QListModel, related_name="qlist_post", blank=True)

    # the scene unity should use in this condition
    unity_scene = models.CharField(verbose_name="unity_scene", max_length=255)

    has_feedback = models.BooleanField(default=False)
    feedback_table = models.ForeignKey('vrct_app.FListModel', on_delete=models.CASCADE, related_name="feedback_table_name", blank=True, null=True)
    
    class Meta:
        """Meta definition for Training Condition."""
        verbose_name = 'training condition'
        verbose_name_plural = 'training conditions'
        app_label = 'vrct_app'

    def __str__(self):
        return self.name

    def get_trainings(self):
        return ", ".join([str(p) for p in self.trainings.all()])

    def get_bot_variants(self):
        return ", ".join([str(p) for p in self.bot_variants.all()])

    def get_questionnaires_pre(self):
        return ",".join([str(p) for p in self.questionnaires_pre.all()])

    def get_questionnaires_post(self):
        return ",".join([str(p) for p in self.questionnaires_post.all()])


class Trainee(VrctBaseModel):
    """Model definition for Trainee. The id of each Trainee must be unique."""

    trainee_id = models.CharField(verbose_name="Trainee id", max_length=255, unique = True)
    trainee_training = models.ForeignKey(Training, on_delete=models.CASCADE, related_name = 'trainee_training')
    trainee_group = models.ForeignKey(ExperimentalGroup, on_delete=models.CASCADE, related_name = 'trainee_exp_group')
    trainee_is_pilot = models.BooleanField(verbose_name="Trainee is pilot", default = False)
    
    class Meta:
        """Meta definition for Trainee."""
        verbose_name = 'Trainee'
        verbose_name_plural = 'Trainees'
        app_label = 'vrct_app'

    def __str__(self):
        return self.trainee_id


class CurrentRunningTraining(VrctBaseModel):
    '''Model definition for Current Running Training'''

    trainee = models.ForeignKey(Trainee, on_delete=models.CASCADE, related_name='running_training_trainee')
    training = models.ForeignKey(Training, on_delete=models.CASCADE, related_name = 'running_training_training')
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE, related_name = 'running_training_condition')
    bot = models.CharField(verbose_name="bot", max_length=255, default='TutorialBot')
    bot_variant = models.CharField(verbose_name="bot variant", max_length=255, default='')

    q_pre_finished = models.BooleanField(default=False)
    qs_pre = models.TextField(default='')
    websocket_running = models.BooleanField(default=False)
    ws_room_name = models.CharField(verbose_name="ws_room_name", max_length=255, default='')
    training_running = models.BooleanField(default=False)
    training_finished = models.BooleanField(default=False)
    qs_post = models.TextField(default='')
    q_post_finished = models.BooleanField(default=False)

    class Meta:
        """Meta definition for Current Running Training."""
        verbose_name = 'Current Running Training'
        verbose_name_plural = 'Current Running Trainings'
        app_label = 'vrct_app'
        unique_together = ['trainee', 'training', 'condition', 'bot', 'bot_variant']