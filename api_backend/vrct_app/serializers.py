from rest_framework import serializers

from vrct_app.models import Training, Trainee, CurrentRunningTraining, ExperimentalGroup, Condition

class TrainingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Training
        fields = "__all__"


class ExperimentalGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExperimentalGroup
        fields = "__all__"


class TraineeSerializer(serializers.ModelSerializer):

    trainee_group = ExperimentalGroupSerializer(read_only=True)

    class Meta:
        model = Trainee
        fields = "__all__"


class ConditionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Condition
        fields = ['id', 'name', 'has_feedback', 'bot', 'get_bot_variants', 'get_trainings']
     


class CurrentRunningTrainingSerializer(serializers.ModelSerializer):

    training = TrainingSerializer(read_only=True)
    trainee = TraineeSerializer(read_only=True)
    condition = ConditionSerializer(read_only=True)

    class Meta:
        model = CurrentRunningTraining
        fields = "__all__"