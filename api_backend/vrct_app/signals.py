import inspect

def populate_models(sender, **kwargs):
    from django.contrib.auth.models import Group, User
    from vrct_app.models.trainings import QListModel, ExperimentalGroup, Training, Condition, Bot, BotVariant
    from vrct_app.models.data import DListModel, TutorialTrainingData
    from vrct_app.models.feedback import FListModel
    import vrct_app.models.questionnaires
    import vrct_app.models.data
    import vrct_app.models.feedback


    # create groups
    g_trainee, created = Group.objects.get_or_create(name='Trainee')
    g_operator, created = Group.objects.get_or_create(name='Training Operator')
    g_admin, created = Group.objects.get_or_create(name='Administrator')
    
    # automatically create vrct supervisor
    new_user, created = User.objects.get_or_create(username='vrct', is_superuser=True, is_staff=True)

    if created:
        new_user.set_password('vrct')
        new_user.save()
        new_user.groups.add(g_admin)
    
    # assign permissions to groups


    # automatically create QList Entries based on defined questionnaires classes in vrct_app.models.questionnaires
    all_obj_in_file = inspect.getmembers(vrct_app.models.questionnaires)
    all_qlists_in_file = []

    consider_these_models_not = ['Trainee', 'Training', 'Condition', 'VrctBaseModel']

    for obj in all_obj_in_file:

        if inspect.isclass(obj[1]):

            if obj[1].__name__ not in consider_these_models_not:
                q_list, created = QListModel.objects.get_or_create(name=obj[1].__name__, created_by_id=new_user.pk)
                all_qlists_in_file.append(obj[1].__name__)

    # check current Qlist and delete all Entries without existing model
    all_qlist_obj = QListModel.objects.all()

    for qlist in all_qlist_obj:
        if qlist.name not in all_qlists_in_file:
            qlist.delete()

    # automatically create DList Entries based on defined data table classes in vrct_app.models.data
    all_obj_in_file = inspect.getmembers(vrct_app.models.data)
    all_dlists_in_file = []

    consider_these_models_not = ['Bot', 'BotVariant', 'Condition', 'DListModel', 'Trainee', 'Training', 'VrctBaseModel']

    for obj in all_obj_in_file:

        if inspect.isclass(obj[1]):

            if obj[1].__name__ not in consider_these_models_not:
                d_list, created = DListModel.objects.get_or_create(name=obj[1].__name__, created_by_id=new_user.pk)
                all_dlists_in_file.append(obj[1].__name__)

    # check current Qlist and delete all Entries without existing model
    all_dlist_obj = DListModel.objects.all()

    for dlist in all_dlist_obj:
        if dlist.name not in all_dlists_in_file:
            dlist.delete()


    # automatically create FList Entries based on defined data table classes in vrct_app.models.feedback
    all_obj_in_file = inspect.getmembers(vrct_app.models.feedback)
    all_flists_in_file = []

    consider_these_models_not = ['Bot', 'BotVariant', 'Condition', 'FListModel', 'Trainee', 'Training', 'VrctBaseModel']

    for obj in all_obj_in_file:

        if inspect.isclass(obj[1]):

            if obj[1].__name__ not in consider_these_models_not:
                f_list, created = FListModel.objects.get_or_create(name=obj[1].__name__, created_by_id=new_user.pk)
                all_flists_in_file.append(obj[1].__name__)

    # check current Flist and delete all Entries without existing model
    all_flist_obj = FListModel.objects.all()

    for flist in all_flist_obj:
        if flist.name not in all_flists_in_file:
            flist.delete()


    # automatically create TutorialTraining
    new_training, created = Training.objects.get_or_create(
        name='TutorialTraining', 
        short_description='This is the exemplary training mentioned in the tutorial. It consists of two Condition with two different bot variants of one bot. In each condition, two questionnaires have to be filled in before and after the vr training. Furthermore, feedback is provided for each training condition.',
        data_table=DListModel.objects.get(name='TutorialTrainingData'),
        created_by=new_user)

    new_experimentalgroup, created = ExperimentalGroup.objects.get_or_create(
        name='TutorialGroup',
        created_by=new_user)

    new_experimentalgroup.trainings.add(new_training)

    new_bot, created = Bot.objects.get_or_create(
        name='vrct', 
        vignette='I am the bot of the exemplary training mentioned in the tutorial. I am very simple and answer communicative or reserved, based on the question of the trainee.',
        created_by=new_user)

    new_botvariantA, created = BotVariant.objects.get_or_create(
        name='#a', 
        short_description='Variant #A of the TutorialBot.',
        bot=new_bot,
        created_by=new_user)

    new_botvariantB, created = BotVariant.objects.get_or_create(
        name='#b', 
        short_description='Variant #B of the TutorialBot.',
        bot=new_bot,
        created_by=new_user)

    new_conditionA, created = Condition.objects.get_or_create(
        name='TutorialCondition A',
        bot=new_bot,
        has_feedback=True,
        feedback_table=FListModel.objects.get(name='TutorialTrainingFeedback'),
        unity_scene='TutorialTrainingScene',
        created_by=new_user)

    new_conditionA.trainings.add(new_training)
    new_conditionA.bot_variants.add(new_botvariantA)
    new_conditionA.questionnaires_pre.add(QListModel.objects.get(name='QuestionnaireOne').pk)
    new_conditionA.questionnaires_pre.add(QListModel.objects.get(name='QuestionnaireTwo').pk)
    new_conditionA.questionnaires_post.add(QListModel.objects.get(name='QuestionnaireOne').pk)
    new_conditionA.questionnaires_post.add(QListModel.objects.get(name='QuestionnaireTwo').pk)

    new_conditionB, created = Condition.objects.get_or_create(
        name='TutorialCondition B',
        bot=new_bot,
        has_feedback=True,
        feedback_table=FListModel.objects.get(name='TutorialTrainingFeedback'),
        unity_scene='TutorialTrainingScene',
        created_by=new_user)

    new_conditionB.trainings.add(new_training)
    new_conditionB.bot_variants.add(new_botvariantB)
    new_conditionB.questionnaires_pre.add(QListModel.objects.get(name='QuestionnaireTwo').pk)
    new_conditionB.questionnaires_pre.add(QListModel.objects.get(name='QuestionnaireOne').pk)
    new_conditionB.questionnaires_post.add(QListModel.objects.get(name='QuestionnaireTwo').pk)
    new_conditionB.questionnaires_post.add(QListModel.objects.get(name='QuestionnaireOne').pk)




