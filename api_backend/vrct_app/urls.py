from django.urls import path

from vrct_app.views import *

urlpatterns = [
    # login, logout, index
    path("", IndexView.as_view(), name="home"),
    path("login", CustomLoginView.as_view(), name="login"),
    path("logout", CustomLogoutView.as_view(), name="logout"),

    # ajax
    path("ajax/get-groups-by-training", ajax_get_groups_by_training, name='ajax_get_groups_by_training'),
    path("ajax/get-trainees-by-training", ajax_get_trainees_by_training, name='ajax_get_trainees_by_training'),
    path("ajax/ajax_get_variants_by_bot", ajax_get_variants_by_bot, name='ajax_get_variants_by_bot'),

    # user
    path("user-list", UserList.as_view(), name="user_list"),
    path("create-user", CreateUserView.as_view(), name="create_user"),

    # Trainee
    path("trainee-list", TraineeList.as_view(), name="trainee_list"),
    path("create-trainee", CreateTraineeView.as_view(), name="create_trainee"),

    # create training
    path("training-list", TrainingList.as_view(), name="training_list"),
    path("create-training/training", CreateTrainingTrainingView.as_view(), name="create_training_training"),
    path("create-training/training/success/<training_id>/", CreateTrainingTrainingSuccessView.as_view(), name="create_training_training_success"),
    path("create-training/bot", CreateTrainingBotView.as_view(), name="create_training_bot"),
    path("create-training/bot/success/<bot_id>/", CreateTrainingBotSuccessView.as_view(), name="create_training_bot_success"),
    path("create-training/bot/variant", CreateBotVariantView.as_view(), name="create_bot_variant"),
    path("create-training/bot/variant/success/<bot_variant_id>/", CreateBotVariantSuccessView.as_view(), name="create_bot_variant_success"),
    path("create-training/condition", CreateTrainingConditionView.as_view(), name="create_training_condition"),
    path("create-training/condition/success/<condition_id>", CreateTrainingConditionSuccessView.as_view(), name="create_training_condition_success"),
    path("create-training/group", CreateTrainingGroupView.as_view(), name="create_training_group"),
    path("create-training/group/success/<group_id>", CreateTrainingGroupSuccessView.as_view(), name="create_training_group_success"),

    # run training
    path("start-training", StartTraining.as_view(), name="start_training"),
    path("start-training/<training_id>/<trainee_id>", ChooseCondition.as_view(), name="choose_condition"),
    path("start-training/run/<training_id>/<trainee_id>/<condition_id>", RunTraining.as_view(), name="run_training"),
    path("start-training/run/app/choose/<training_id>/<trainee_id>/<condition_id>", RunTrainingWithAppChooseBotVariant.as_view(), name="run_training_with_app_choose"),
    path("start-training/run/app/websocket/<training_id>/<trainee_id>/<condition_id>/<room_name>", RunTrainingWithAppWebsocket.as_view(), name="run_training_with_app_websocket"),

    # feedback
    path("feedback/start/<training_id>/<trainee_id>/<condition_id>", ShowFeedback.as_view(), name="show_feedback"),

    # export data
    path("export-data", StartExport.as_view(), name="export_data"),
    path("export-data/<training_id>", ExportData.as_view(), name="export_training_data"),
    path("export-data/<training_id>/<model_name>/<type>", export_csv, name="export_export"),

    # rest api
    path('api/api-token-auth/', CustomAuthToken.as_view()),
    path('api/current-trainings-list/', current_running_trainings_list),
    path('api/current-trainings-list-unity/', current_running_trainings_list_unity),
    path('api/update-training-running/', update_training_running),
    path('api/update-training-finished/', update_training_finished),
    path('api/save-trial-data/', save_trial_data),
    path('api/delete-training-data/', delete_training_data),
]