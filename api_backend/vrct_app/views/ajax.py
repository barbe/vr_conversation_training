from django.http import JsonResponse

from vrct_app.models import ExperimentalGroup, Trainee,Bot, BotVariant

def ajax_get_groups_by_training(request):

    training_id = request.GET.get('training_id', None)
    data = ExperimentalGroup.objects.filter(trainings=training_id).values()

    return JsonResponse(list(data), safe=False)

def ajax_get_trainees_by_training(request):

    training_id = request.GET.get('training_id', None)
    data = Trainee.objects.filter(trainee_training=training_id).values()

    return JsonResponse(list(data), safe=False)

def ajax_get_variants_by_bot(request):

    bot_id = request.GET.get('bot_id', None)
    bot = Bot.objects.get(pk=bot_id)
    data = BotVariant.objects.filter(bot=bot).values()

    return JsonResponse(list(data), safe=False)