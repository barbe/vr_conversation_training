from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, reverse
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect, HttpResponse
import csv
from django.apps import apps

from datetime import datetime

from vrct_app.forms import StartExportForm

from vrct_app.models import Training, Condition, DListModel, QListModel, FListModel

# Start Data export by choosing training
class StartExport(LoginRequiredMixin, TemplateView):

    template_name = "vrct_app/_data/start_export.html"
    form_class = StartExportForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):

        context = {
            'training_id': request.POST['training']
        }

        return HttpResponseRedirect(reverse('export_training_data', kwargs=context))


class ExportData(LoginRequiredMixin, TemplateView):

    template_name = "vrct_app/_data/export_data.html"

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):

        context = super().get_context_data(**kwargs)
        training_id = context['training_id']
        
        data_model_name = ''

        # check data exists
        try:

            # get correct model_name
            pk_dlistmodel = Training.objects.filter(pk=training_id).values_list('data_table', flat=True).get()
            model_name = DListModel.objects.filter(pk=pk_dlistmodel).values_list('name', flat=True).get()
            data_model_name = model_name
        except:
            print('It was not possible to find the DataModel!')

        # get all Conditions
        training_conditions = Condition.objects.filter(trainings=training_id)

        q_names = []
        f_names = []

        # check for each condition if there is already any data in the database (questionnaires, training data)
        for cond in training_conditions:

            # get QListModels of questionnaire
            q_pre = Condition.objects.filter(name=cond).values('questionnaires_pre')
            q_post = Condition.objects.filter(name=cond).values('questionnaires_post')

            if len(q_pre) > 0:
                for q in q_pre:

                    # get name of Qiestionnaires (str)
                    q_name = QListModel.objects.get(pk=q['questionnaires_pre']).name

                    # check if q already in q_names
                    if q_name not in q_names:

                        # add q to list
                        q_names.append(q_name)
                        
            if len(q_post) > 0:
                for q in q_post:
                    q_name = QListModel.objects.get(pk=q['questionnaires_post']).name
                    if q_name not in q_names:
                        q_names.append(q_name)

            if cond.has_feedback:

                try:

                    # get correct model_name
                    model_name = FListModel.objects.get(name=cond.feedback_table).name # trainingData string = name

                    f_names.append(model_name)


                except:

                    print('It was not possible to find the DataModel!')

        context = {
            'training_id': training_id,
            'training_name': Training.objects.get(id=training_id).name,
            'data_model_name': data_model_name,
            'q_names': q_names,
            'f_names': f_names
        }

        return render(request, self.template_name, {'context': context})


def export_csv(request, training_id, model_name, type):

    # get current datetime
    now = datetime.now()
    dt_string = now.strftime("%d%m%Y_%H%M%S")

    # define filename
    filename = model_name + "_" + dt_string

    content_disposition = f'attachment; filename={filename}.csv'

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = content_disposition

    # get correct model
    if type == "data":
        # get correct model based on model_name
        DModel = apps.get_model(app_label='vrct_app', model_name=model_name)

        # get all data
        q_set = DModel.objects.all()

    elif type == "questionnaire":
        QModel = apps.get_model(app_label='vrct_app', model_name=model_name)

        # get all data
        q_set = QModel.objects.filter(training=Training.objects.get(pk=training_id)).all()

    else:
        FModel = apps.get_model(app_label='vrct_app', model_name=model_name)

        # get all data
        q_set = FModel.objects.filter(training=Training.objects.get(pk=training_id)).all()


    # write data to csv
    opts = q_set.model._meta
    writer = csv.writer(response)
    field_names = [field.name for field in opts.fields]
    # Write a first row with header information
    writer.writerow(field_names)
    # Write data rows
    for obj in q_set:
        writer.writerow([getattr(obj, field, None) for field in field_names])

    return response
