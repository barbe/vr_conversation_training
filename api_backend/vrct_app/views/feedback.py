from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView

from vrct_app.models import Trainee, Training, Condition, CurrentRunningTraining, TutorialTrainingFeedback, TutorialTrainingData


# ToDo: definitions to fill in the feedback model
def fill_tutorial_training_feedback(trainee_id, training_id, condition_id, user):
    """Calculate necessary variables for tutorial training feedback model"""

    # get correct data
    training_data = TutorialTrainingData.objects.filter(trainee=trainee_id, training=training_id, condition=condition_id).all()

    print(training_data)
    good = training_data.filter(operator_behavior="communicative").count()
    bad = training_data.filter(operator_behavior="reserved").count()

    # get/calculate data
    n_communicative_behavior = (good/good + bad)*100,
    n_reserved_behavior = (bad/good + bad)*100,

    if n_communicative_behavior[0] > 0:
        good_trial_question_content = training_data.filter(operator_behavior="communicative").first().question_content
        good_trial_answer_content = training_data.filter(operator_behavior="communicative").first().answer_content
    else:
        good_trial_question_content = "",
        good_trial_answer_content = "",

    if n_reserved_behavior[0] > 0:
        bad_trial_question_content = training_data.filter(operator_behavior="reserved").first().question_content
        bad_trial_answer_content = training_data.filter(operator_behavior="reserved").first().answer_content
    else:
        bad_trial_question_content = "",
        bad_trial_answer_content = "",

    feedback_data = TutorialTrainingFeedback(
        trainee = Trainee.objects.get(id=trainee_id),
        training = Training.objects.get(id=training_id),
        condition = Condition.objects.get(id=condition_id),
        bot = training_data[0].bot,
        bot_variant = training_data[0].bot_variant,
        n_communicative_behavior = n_communicative_behavior[0],
        n_reserved_behavior = n_reserved_behavior[0],
        good_trial_question_content = good_trial_question_content,
        good_trial_answer_content = good_trial_answer_content,
        bad_trial_question_content = bad_trial_question_content,
        bad_trial_answer_content = bad_trial_answer_content,
        created_by_id = user.id
    )

    feedback_data.save()


# Feedback View
class ShowFeedback(LoginRequiredMixin, TemplateView):

    template_name = ""

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):

        context = super().get_context_data(**kwargs)
        training_id = context['training_id']
        trainee_id = context['trainee_id']
        condition_id = context['condition_id']

        # get correct Running Training
        current_training = CurrentRunningTraining.objects.get(trainee=trainee_id, training=training_id, condition=condition_id)

        # fill in feedback model and get correct template
        if current_training.training.name == "TutorialTraining":

            # you have to create new template(s) if you define a new training in order to be able to adjust the feedback individually for your training
            self.template_name = "vrct_app/_feedback/tutorialtraining_feedback.html"

            # check if feedback-model already exists
            try: 
                
                TutorialTrainingFeedback.objects.get(trainee=trainee_id, training=training_id, condition=condition_id)

            except:

                fill_tutorial_training_feedback(trainee_id, training_id, condition_id, user=request.user)

            # else: get feedback model
            feedback_model = TutorialTrainingFeedback.objects.get(trainee=trainee_id, training=training_id, condition=condition_id)

        ## if you create a new training, you must hardcode here your feedback definition(s) for your training

        else:

            print("There exists no definition for this Training. Please hard code it...")


        context = {
            'current_training': current_training,
            'feedback_model': feedback_model
        }


        return render(request, self.template_name, {'context': context})
