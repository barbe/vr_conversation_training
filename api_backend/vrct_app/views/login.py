from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.forms import AuthenticationForm

# ----
# INDEX
# -----
class IndexView(LoginRequiredMixin, TemplateView):

    template_name = "vrct_app/index.html"

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        return context


# -----
# LOGIN
# -----
class CustomLoginView(LoginView):

    template_name = "vrct_app/login.html"
    form_class = AuthenticationForm



# -----
# LOGOUT
# -----
class CustomLogoutView(LogoutView):

    template_name = "vrct_app/logout.html"
