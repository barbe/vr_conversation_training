from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from django.apps import apps

from vrct_app.models import Training, Trainee, Condition, Bot, BotVariant, CurrentRunningTraining, DListModel


def get_operator_variables(training_id):
    '''get name, choices and type of operator variables'''
    data = []

    #try:

    pk_dlistmodel = Training.objects.filter(pk=training_id).values_list('data_table', flat=True).get()
    model_name = DListModel.objects.filter(pk=pk_dlistmodel).values_list('name', flat=True).get()

    DModel = apps.get_model(app_label='vrct_app', model_name=model_name)

    field_names = [f.name for f in DModel._meta.get_fields() if "operator_" in f.name]
    field_choices = [f.choices for f in DModel._meta.get_fields() if "operator_" in f.name]
    field_types = [f.get_internal_type() for f in DModel._meta.get_fields() if "operator_" in f.name] 

    field_verbose_name = [f.verbose_name for f in DModel._meta.get_fields() if "operator_" in f.name]
    field_help_text = [f.help_text for f in DModel._meta.get_fields() if "operator_" in f.name]  

    # format choices
    # we need it in the following format:
    # options: [{label: 'Option 1', value: 'op1'},...]
    options = []

    for i, choices in enumerate(field_choices):

        data_part_main = []

        if choices != None:

            for this_choices in choices:

                data_part = {
                    'label': this_choices[1],
                    'value': this_choices[0]
                }

                data_part_main.append(data_part)

        options.append(data_part_main)


    for i, name in enumerate(field_names):

        data_part = {
            'name': name,
            'verbose_name': field_verbose_name[i],
            'help_text': field_help_text[i],
            'options': options[i],
            'type': field_types[i]
        }

        data.append(data_part)
    
    return data

    #except:

        #return data


def get_bot_variables(training_id):
    '''get name, choices and type of bot variables'''
    data = []

    try:

        pk_dlistmodel = Training.objects.filter(pk=training_id).values_list('data_table', flat=True).get()
        model_name = DListModel.objects.filter(pk=pk_dlistmodel).values_list('name', flat=True).get()

        DModel = apps.get_model(app_label='vrct_app', model_name=model_name)

        field_names = [f.name for f in DModel._meta.get_fields() if "cs_" in f.name]
        field_choices = [f.choices for f in DModel._meta.get_fields() if "cs_" in f.name]
        field_types = [f.get_internal_type() for f in DModel._meta.get_fields() if "cs_" in f.name] 

        for i, name in enumerate(field_names):

            data_part = {
                'name': name,
                'choices': field_choices[i],
                'type': field_types[i]
            }

            data.append(data_part)
        
        return data

    except:

        return data


class CustomAuthToken(ObtainAuthToken):
    '''REST API TOKEN AUTHENTICATION'''

    def post(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']

        # get or create token
        token, created = Token.objects.get_or_create(user=user)

        # get user group
        l = list(user.groups.values_list('name', flat = True))

        print(l)

        # get correct Group
        if "Administrator" in l:
            user_group = "Administrator"
        elif "Training Operator" in l:
            user_group = "Training Operator"
        else:
            user_group = "Trainee"

        # return the data
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'username': user.username,
            'usergroup': user_group
        })


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def current_running_trainings_list(request):
    """
    Get all necessary info from current running trainings
    """

    if request.method == 'GET':

        data = {
            'Items': []
        }
        
        trainings = CurrentRunningTraining.objects.filter(training_finished=False, q_pre_finished=True, training_running=False).all()

        for training in trainings:

            data_part = {
                'id': training.id,
                'training': training.training.name,
                'trainee': training.trainee.trainee_id,
                'condition': training.condition.name,
                'bot': training.bot,
                'bot_variant': training.bot_variant,
                'ws_room': training.ws_room_name,
                'unity_scene': training.condition.unity_scene,

                # data about variables in the model which must be categorized by the operator
                'operator_variables': get_operator_variables(training.training.id),

                # we need the bot_variables
                "bot_variables": get_bot_variables(training.training.id)

            }

            data['Items'].append(data_part)
        
        return JsonResponse(data, safe=False)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def current_running_trainings_list_unity(request):
    """
    Get all necessary info from current running trainings
    """

    if request.method == 'GET':

        data = {
            'Items': []
        }
        
        trainings = CurrentRunningTraining.objects.filter(training_finished=False, q_pre_finished=True, training_running=True).all()

        for training in trainings:

            data_part = {
                'id': training.id,
                'training': training.training.name,
                'trainee': training.trainee.trainee_id,
                'condition': training.condition.name,
                'bot': training.bot,
                'bot_variant': training.bot_variant,
                'ws_room': training.ws_room_name,
                'unity_scene': training.condition.unity_scene,

            }

            data['Items'].append(data_part)
        
        return JsonResponse(data, safe=False)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_training_running(request):
    """
    Update current running training: training_running
    """

    if request.method == 'PUT':

        try:

            CurrentRunningTraining.objects.filter(pk=request.data['pk']).update(training_running=request.data['value'])

            return Response(status.HTTP_200_OK)

        except:

            return Response(status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_training_finished(request):
    """
    Update current running finished: training_finished
    """

    if request.method == 'PUT':

        try:

            # ToDo: update as finished only if there is at least one trial in database!

            CurrentRunningTraining.objects.filter(pk=request.data['pk']).update(training_finished=request.data['value'])

            return Response(status.HTTP_200_OK)

        except:

            return Response(status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def save_trial_data(request):
    """
    Save one trial in correct training data model.
    """

    if request.method == 'POST':

        try:

            basic_data = request.data['basic_data']
            data = request.data['data'] 

            # get correct model_name
            pk_dlistmodel = Training.objects.filter(name=basic_data['training']).values_list('data_table', flat=True).get()
            model_name = DListModel.objects.filter(pk=pk_dlistmodel).values_list('name', flat=True).get()

            # get data model
            DModel = apps.get_model(app_label='vrct_app', model_name=model_name)

            # get foreignkeys
            trainee = Trainee.objects.get(trainee_id=basic_data['trainee'])
            training = Training.objects.get(name=basic_data['training'])
            condition = Condition.objects.get(name=basic_data['condition'])
            bot = Bot.objects.get(name=basic_data['bot'])
            bot_variant = BotVariant.objects.get(name=basic_data['bot_variant'])

            # save
            newD = DModel(created_by=request.user, trainee=trainee, training=training, condition=condition, bot=bot, bot_variant=bot_variant, **data)
            newD.save()

            return Response(status.HTTP_200_OK)

        except:

            return Response(status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def delete_training_data(request):
    """
    Delete all trials of a trainee in a specific condition
    """

    if request.method == 'POST':

        try:

            data = request.data

            # get correct model_name
            pk_dlistmodel = Training.objects.filter(name=data['training']).values_list('data_table', flat=True).get()
            model_name = DListModel.objects.filter(pk=pk_dlistmodel).values_list('name', flat=True).get()

            # get data model
            DModel = apps.get_model(app_label='vrct_app', model_name=model_name)

            # get foreignkeys
            trainee = Trainee.objects.get(trainee_id=data['trainee'])
            training = Training.objects.get(name=data['training'])
            condition = Condition.objects.get(name=data['condition'])
            bot = Bot.objects.get(name=data['bot'])
            bot_variant = BotVariant.objects.get(name=data['bot_variant'])

            # delete
            all_data = DModel.objects.filter(trainee=trainee, training=training, condition=condition, bot=bot, bot_variant=bot_variant).all()
            all_data.delete()

            return Response(status.HTTP_200_OK)

        except:

            return Response(status.HTTP_400_BAD_REQUEST)