from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, reverse
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.apps import apps

import ast
import random
import string

from vrct_app.forms import StartTrainingForm, get_q_form_unbound, get_q_form_bound
from vrct_app.models import Condition, Training, Trainee, DListModel, QListModel, FListModel, CurrentRunningTraining

# Start Training Session and choose Training and Trainee
class StartTraining(LoginRequiredMixin, TemplateView):

    template_name = "vrct_app/_run_training/start_training.html"
    form_class = StartTrainingForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):

        context = {
            'training_id': request.POST['training'],
            'trainee_id': request.POST['trainee']
        }

        return HttpResponseRedirect(reverse('choose_condition', kwargs=context))

# Choose condition
class ChooseCondition(LoginRequiredMixin, TemplateView):

    template_name = "vrct_app/_run_training/choose_condition.html"

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):

        context = super().get_context_data(**kwargs)
        training_id = context['training_id']
        trainee_id = context['trainee_id']

        # get all Conditions
        training_conditions = Condition.objects.filter(trainings=training_id)
        
        #create data
        data = {}

        # check for each condition if there is already any data in the database (questionnaires, training data)
        for cond in training_conditions:

            # create nested dictionary
            data[cond.name] = {}
            data[cond.name]['pk'] = cond.pk
            data[cond.name]['Questionnaires (Pre)'] = {}
            data[cond.name]['Training Data'] = {}
            data[cond.name]['Questionnaires (Post)'] = {}

            # check data exists
            try:

                # get correct model_name
                pk_dlistmodel = Training.objects.filter(pk=training_id).values_list('data_table', flat=True).get()
                model_name = DListModel.objects.filter(pk=pk_dlistmodel).values_list('name', flat=True).get() # trainingData string = name

                # get correct model based on model_name
                DModel = apps.get_model(app_label='vrct_app', model_name=model_name)

                number_trials = DModel.objects.filter(training=training_id, trainee=trainee_id, condition=cond).count()

                if number_trials > 0:
                    data_boolean = True
                else:
                    data_boolean = False

                data[cond.name]['Training Data']['number_trials'] = number_trials
                data[cond.name]['training_condition_finished'] = data_boolean

            except:

                print('It was not possible to find the DataModel!')

            # get QListModels of questionnaire
            q_pre = Condition.objects.filter(name=cond).values('questionnaires_pre')
            q_post = Condition.objects.filter(name=cond).values('questionnaires_post')

            # loop all pre questionnaires in order to check if one already exists
            # ToDo: check if q_pre > 0

            data[cond.name]['n_q_pre'] = len(q_pre)
            data[cond.name]['q_pre_finished_all'] = True

            if len(q_pre) > 0:
                for q in q_pre:

                    # get name of Qiestionnaires (str)
                    q_name = QListModel.objects.get(pk=q['questionnaires_pre']).name

                    # get correct model based on model name
                    QModel = apps.get_model(app_label='vrct_app', model_name=q_name)

                    # check if questionnaire in this condition, training and timepoint already exists
                    boole = QModel.objects.filter(trainee=trainee_id, training=training_id, condition=cond, timepoint='pre').exists()

                    # if one q do not exists, q_pre is not finished yet
                    if boole == False:
                        data[cond.name]['q_pre_finished_all'] = False

                    # add info to data
                    data[cond.name]['Questionnaires (Pre)'][q_name] = boole


            # loop all post questionnaires in order to check if one already exists
            data[cond.name]['n_q_post'] = len(q_post)
            data[cond.name]['q_post_finished_all'] = True
            if len(q_post) > 0:
                for q in q_post:
                    q_name = QListModel.objects.get(pk=q['questionnaires_post']).name
                    QModel = apps.get_model(app_label='vrct_app', model_name=q_name)
                    boole = QModel.objects.filter(trainee=trainee_id, training=training_id, condition=cond, timepoint='post').exists()
                    if boole == False:
                        data[cond.name]['q_post_finished_all'] = False
                    data[cond.name]['Questionnaires (Post)'][q_name] = boole

            # check feedback
            data[cond.name]['has_feedback'] = cond.has_feedback

            if cond.has_feedback:

                try:

                    # get correct model_name
                    model_name = FListModel.objects.get(name=cond.feedback_table).name # trainingData string = name

                    # get correct model based on model_name
                    DModel = apps.get_model(app_label='vrct_app', model_name=model_name)

                    f_exists= DModel.objects.filter(training=training_id, trainee=trainee_id, condition=cond).exists()

                    if f_exists:
                        data[cond.name]['feedback_finished'] = True
                    else:
                        data[cond.name]['feedback_finished'] = False

                except:

                    print('It was not possible to find the DataModel!')

        context = {
            'training_id': training_id,
            'training_name': Training.objects.get(id=training_id).name,
            'trainee_id': trainee_id,
            'trainee_name': Trainee.objects.get(id=trainee_id).trainee_id,
            'data': data
        }

        return render(request, self.template_name, {'context': context})

    def post(self, request, *args, **kwargs):

        data = request.POST
        condition_to_run = data.get("start")

        # we get context as string and must convert it back to dict
        context = ast.literal_eval(data.get("context"))

        if not CurrentRunningTraining.objects.filter(
            trainee=Trainee.objects.get(pk=int(context['trainee_id'])), 
            training=Training.objects.get(pk=int(context['training_id'])), 
            condition=Condition.objects.get(name=condition_to_run)
            ).exists():

                # create new running training entry 
                new_current_training = CurrentRunningTraining.objects.create(
                    trainee=Trainee.objects.get(pk=int(context['trainee_id'])),
                    training=Training.objects.get(pk=int(context['training_id'])),
                    condition=Condition.objects.get(name=condition_to_run),
                    bot = Condition.objects.get(name=condition_to_run).bot.name,
                    q_pre_finished = context['data'][condition_to_run]['q_pre_finished_all'],
                    qs_pre = context['data'][condition_to_run]['Questionnaires (Pre)'], 
                    training_finished = context['data'][condition_to_run]['training_condition_finished'],
                    q_post_finished = context['data'][condition_to_run]['q_post_finished_all'],
                    qs_post = context['data'][condition_to_run]['Questionnaires (Post)'], 
                    created_by = request.user
                )

                new_current_training.save()

        c = {
            'trainee_id': context['trainee_id'],
            'training_id': context['training_id'],
            'condition_id': Condition.objects.get(name=condition_to_run).pk
        }

        return HttpResponseRedirect(reverse('run_training', kwargs=c))


def check_current_training(current_training, trainee_id, training_id, condition_id):
    '''check status of current_training and decide what has to be shown'''

    # check status of current_training and decide what has to be shown
    if not current_training.q_pre_finished and not current_training.training_finished and not current_training.q_post_finished:

        # get correct model based on q_name
        q_name = None
        form = None

        qs_dict = ast.literal_eval(current_training.qs_pre)
        for key, value in qs_dict.items():
            print(key, '->', value)
            if not value:
                q_name = key
                DModel = apps.get_model(app_label='vrct_app', model_name=q_name)
                form = get_q_form_unbound(DModel)
                break

        # show questionnaires pre
        context = {
            'check_ok': True,
            'q_pre': True,
            'training': False,
            'q_post': False,
            'form': form,
            'q_name': q_name,
            'qs_dict': qs_dict,
            'current_training': current_training,
            'trainee_id': trainee_id,
            'training_id': training_id,
            'condition_id': condition_id,
            'bot_choosen': False
        }

    elif current_training.q_pre_finished and not current_training.training_finished and not current_training.q_post_finished:

        if current_training.ws_room_name != '':

            bot_choosen = True

        else:

            bot_choosen = False


        # show start app
        context = {
            'check_ok': True,
            'q_pre': False,
            'training': True,
            'q_post': False,
            'form': None,
            'q_name': None,
            'qs_dict': None,
            'current_training': current_training,
            'trainee_id': trainee_id,
            'training_id': training_id,
            'condition_id': condition_id,
            'bot_choosen': bot_choosen
        }

    elif current_training.q_pre_finished and current_training.training_finished and not current_training.q_post_finished:

        # get correct model based on q_name
        q_name = None
        form = None

        qs_dict = ast.literal_eval(current_training.qs_post)
        for key, value in qs_dict.items():
            print(key, '->', value)
            if not value:
                q_name = key
                DModel = apps.get_model(app_label='vrct_app', model_name=q_name)
                form = get_q_form_unbound(DModel)
                break

        # show questionnaires post
        context = {
            'check_ok': True,
            'q_pre': False,
            'training': False,
            'q_post': True,
            'form': form,
            'q_name': q_name,
            'qs_dict': qs_dict,
            'current_training': current_training,
            'trainee_id': trainee_id,
            'training_id': training_id,
            'condition_id': condition_id,
            'bot_choosen': True
        }

    elif current_training.q_pre_finished and current_training.training_finished and current_training.q_post_finished:

        print('if 4')
        # show training condition finished
        context = {
            'check_ok': True,
            'q_pre': False,
            'training': False,
            'q_post': False,
            'form': None,
            'q_name': None,
            'qs_dict': None,
            'current_training': current_training,
            'trainee_id': trainee_id,
            'training_id': training_id,
            'condition_id': condition_id,
            'bot_choosen': True
        }

    else:

        # show something wnt wrong, check your data
        context = {
            'check_ok': False,
            'q_pre': False,
            'training': False,
            'q_post': False,
            'form': None,
            'q_name': None,
            'qs_dict': None,
            'current_training': current_training,
            'trainee_id': trainee_id,
            'training_id': training_id,
            'condition_id': condition_id,
            'bot_choosen': False
        }

    print(context)

    return context


# run training
class RunTraining(LoginRequiredMixin, TemplateView):

    template_name = "vrct_app/_run_training/run_training.html"

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):

        context = super().get_context_data(**kwargs)
        training_id = context['training_id']
        trainee_id = context['trainee_id']
        condition_id = context['condition_id']

        # get correct Running Training
        current_training = CurrentRunningTraining.objects.get(trainee=trainee_id, training=training_id, condition=condition_id)

        # check it
        context = check_current_training(current_training, trainee_id, training_id, condition_id)

        return render(request, self.template_name, {'context': context})

    def post(self, request, *args, **kwargs):

        data = request.POST
        trainee_id = int(data['trainee_id']) 
        training_id=int(data['training_id']) 
        condition_id=int(data['condition_id'])
        q_pre=data['q_pre']
        qs_dict=ast.literal_eval(data['qs_dict'])
        q_name=data['q_name']
        current_training=int(data['current_training']) 

        if q_pre == 'True':
            timepoint = 'pre'
        else:
            timepoint = 'post' 

        # check if questionnaire was shown
        if q_name is not None:

            form = get_q_form_bound(apps.get_model(app_label='vrct_app', model_name=data['q_name']), request)

            if form.is_valid():

                # save questionnaire data
                q = form.save(commit=False)
                q.trainee = Trainee.objects.get(pk=trainee_id)
                q.training = Training.objects.get(pk=training_id)
                q.condition = Condition.objects.get(pk=condition_id)
                q.created_by = request.user
                q.timepoint = timepoint
                q.save()

                # update CurrentRunningTraining
                c_t = CurrentRunningTraining.objects.get(pk=current_training)
                qs_dict[q_name] = True

                if q_pre == 'True':
                    # change qs_dict in the right way
                    c_t.qs_pre = str(qs_dict)
                    c_t.save()

                    q_name = None

                    for key, value in qs_dict.items():
                        print(key, '->', value)
                        if not value:
                            q_name = key
                            DModel = apps.get_model(app_label='vrct_app', model_name=q_name)
                            form = get_q_form_unbound(DModel)
                            break

                    # if no more open questionnaires, update current training.qs_pre_finished
                    if q_name == None:
                        c_t.q_pre_finished = True
                        c_t.save()

                    # re-check current_training
                    context = check_current_training(c_t, trainee_id, training_id, condition_id)

                else:

                    # change qs_dict in the right way
                    c_t.qs_post = str(qs_dict)
                    c_t.save()

                    q_name = None

                    for key, value in qs_dict.items():
                        print(key, '->', value)
                        if not value:
                            q_name = key
                            DModel = apps.get_model(app_label='vrct_app', model_name=q_name)
                            form = get_q_form_unbound(DModel)
                            break

                    # if no more open questionnaires, update current training.qs_pre_finished
                    if q_name == None:
                        c_t.q_post_finished = True
                        c_t.save()

                    # re-check current_training
                    context = check_current_training(c_t, trainee_id, training_id, condition_id)

                # show the website
                return render(request, self.template_name, {'context': context})

            # form is not valid
            else:

                # re-check current_training
                c_t = CurrentRunningTraining.objects.get(pk=current_training)
                context = check_current_training(c_t, trainee_id, training_id, condition_id)

                # show the website
                return render(request, self.template_name, {'context': context})

        # no questionnaire
        else:

            pass


# run training with app - websocket
class RunTrainingWithAppChooseBotVariant(LoginRequiredMixin, TemplateView):

    template_name = "vrct_app/_run_training/run_training_with_app_choose.html"

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):

        context = super().get_context_data(**kwargs)
        training_id = context['training_id']
        trainee_id = context['trainee_id']
        condition_id = context['condition_id']

        # get correct Running Training
        current_training = CurrentRunningTraining.objects.get(trainee=trainee_id, training=training_id, condition=condition_id)

        bot_variants = Condition.objects.get(pk=condition_id).bot_variants.all()

        context = {
            'bot_variants': bot_variants,
            'current_training': current_training,
            'trainee_id': trainee_id,
            'training_id': training_id,
            'condition_id': condition_id
        }

        # show the website
        return render(request, self.template_name, {'context': context})

    def post(self, request, *args, **kwargs):

        data = request.POST

        bot_variant_name = data['start']
        trainee_id = int(data['trainee_id']) 
        training_id = int(data['training_id']) 
        condition_id = int(data['condition_id'])

        # create path für ws: random strings
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(6))
        ws_path = result_str + data['trainee_id'] + data['training_id'] + data['condition_id']

        # save choosen bot_variant in the CurrentRunningTraining instance
        current_running_training = CurrentRunningTraining.objects.get(trainee=trainee_id, training=training_id, condition=condition_id)
        current_running_training.bot_variant = bot_variant_name
        current_running_training.ws_room_name= ws_path
        current_running_training.save()

        c = {
            'trainee_id': trainee_id,
            'training_id': training_id,
            'condition_id': condition_id,
            'room_name': ws_path
        }

        return HttpResponseRedirect(reverse('run_training_with_app_websocket', kwargs=c))

# run training with app - websocket
class RunTrainingWithAppWebsocket(LoginRequiredMixin, TemplateView):

    template_name = "vrct_app/_run_training/run_training_with_app_websocket.html"

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):

        context = super().get_context_data(**kwargs)
        training_id = context['training_id']
        trainee_id = context['trainee_id']
        condition_id = context['condition_id']
        room_name = context['room_name']

        # get correct Running Training
        current_training = CurrentRunningTraining.objects.get(trainee=trainee_id, training=training_id, condition=condition_id)

        context = {
            'current_training': current_training,
            'trainee_id': trainee_id,
            'training_id': training_id,
            'condition_id': condition_id
        }

        # show the website
        return render(request, self.template_name, {'context': context, 'room_name': room_name})

    def post(self, request, *args, **kwargs):

        data = request.POST

        trainee_id = int(data['trainee_id']) 
        training_id = int(data['training_id']) 
        condition_id = int(data['condition_id'])
        room_name = data['room_name']

         # get correct Running Training
        current_training = CurrentRunningTraining.objects.get(trainee=trainee_id, training=training_id, condition=condition_id)

        if current_training.training_finished:

            c = {
                'trainee_id': trainee_id,
                'training_id': training_id,
                'condition_id': condition_id
            }

            return HttpResponseRedirect(reverse('run_training', kwargs=c))

        else:

            c = {
                'current_training': current_training,
                'trainee_id': trainee_id,
                'training_id': training_id,
                'condition_id': condition_id
            }

            return render(request, self.template_name, {'context': c, 'room_name': room_name})

