from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import FormView, TemplateView, ListView
from django.urls import reverse_lazy

from vrct_app.models import Trainee
from vrct_app.forms import TraineeForm

# -------
# MANAGE TraineeS
# -------

# VIEW TraineeS
class TraineeList(LoginRequiredMixin, ListView):

    template_name = "vrct_app/_trainees/trainee_list.html"
    queryset = Trainee.objects.all().order_by('pk')
    context_object_name = 'trainee_list'
    paginate_by = 25


# CREATE Trainee
class CreateTraineeView(LoginRequiredMixin, FormView):

    template_name = "vrct_app/_trainees/create_trainee.html"
    form_class = TraineeForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.instance.created_by = self.request.user
        new_trainee = form.save()
        self.trainee_id = new_trainee.trainee_id
        return super(CreateTraineeView, self).form_valid(form)

    def get_success_url(self):
         return reverse_lazy('trainee_list')
