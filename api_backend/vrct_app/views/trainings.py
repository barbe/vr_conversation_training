from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import FormView, TemplateView, ListView
from django.urls import reverse_lazy

from vrct_app.models import Training, Condition, Bot, BotVariant, ExperimentalGroup
from vrct_app.forms import TrainingForm, ConditionForm, BotForm, BotVariantForm, GroupForm

# -------
# MANAGE Trainings
# -------

# VIEW Trainings
class TrainingList(LoginRequiredMixin, ListView):

    template_name = "vrct_app/_trainings/trainings_list.html"
    queryset = Training.objects.all().order_by('pk')
    context_object_name = 'training_list'
    paginate_by = 25

# CREATE TRAINING - Training
class CreateTrainingTrainingView(LoginRequiredMixin, FormView):

    template_name = "vrct_app/_trainings/create_training_training.html"
    form_class = TrainingForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs):
        context = super(CreateTrainingTrainingView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.instance.created_by = self.request.user
        new_training = form.save()
        self.training_id = new_training.pk
        return super(CreateTrainingTrainingView, self).form_valid(form)

    def get_success_url(self):
         return reverse_lazy('create_training_training_success', kwargs={'training_id': self.training_id})


class CreateTrainingTrainingSuccessView(LoginRequiredMixin,  TemplateView):

    template_name = "vrct_app/_trainings/create_training_training_success.html"

    def get_context_data(self, **kwargs):
        context = super(CreateTrainingTrainingSuccessView, self).get_context_data(**kwargs)
        context['training'] = Training.objects.get(pk=self.kwargs['training_id'])

        return context


# CREATE Bot
class CreateTrainingBotView(LoginRequiredMixin, FormView):

    template_name = "vrct_app/_trainings/create_training_bot.html"
    form_class = BotForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.instance.created_by = self.request.user
        new_bot = form.save()
        self.bot_id = new_bot.pk
        return super(CreateTrainingBotView, self).form_valid(form)

    def get_success_url(self):
         return reverse_lazy('create_training_bot_success', kwargs={'bot_id': self.bot_id})


class CreateTrainingBotSuccessView(LoginRequiredMixin,  TemplateView):

    template_name = "vrct_app/_trainings/create_training_bot_success.html"

    def get_context_data(self, **kwargs):
        context = super(CreateTrainingBotSuccessView, self).get_context_data(**kwargs)
        context['bot'] = Bot.objects.get(pk=self.kwargs['bot_id'])

        return context

# CREATE BOT - bot variant
class CreateBotVariantView(LoginRequiredMixin, FormView):

    template_name = "vrct_app/_trainings/create_training_bot_variant.html"
    form_class = BotVariantForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.instance.created_by = self.request.user
        new_bot_variant = form.save()
        self.bot_variant_id = new_bot_variant.pk
        return super(CreateBotVariantView, self).form_valid(form)

    def get_success_url(self):
         return reverse_lazy('create_bot_variant_success', kwargs={'bot_variant_id': self.bot_variant_id})


class CreateBotVariantSuccessView(LoginRequiredMixin,  TemplateView):

    template_name = "vrct_app/_trainings/create_training_bot_variant_success.html"

    def get_context_data(self, **kwargs):
        context = super(CreateBotVariantSuccessView, self).get_context_data(**kwargs)
        context['bot_variant'] = BotVariant.objects.get(pk=self.kwargs['bot_variant_id'])

        return context


# CREATE TRAINING - Training Condition
class CreateTrainingConditionView(LoginRequiredMixin, FormView):

    template_name = "vrct_app/_trainings/create_training_condition.html"
    form_class = ConditionForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super().get_initial()
        initial['bot'] = 1
        initial['bot_variants'] = 0

        return initial

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.instance.created_by = self.request.user
        new_condition = form.save()
        self.condition_id = new_condition.pk
        return super(CreateTrainingConditionView, self).form_valid(form)

    def get_success_url(self):
         return reverse_lazy('create_training_condition_success', kwargs={'condition_id': self.condition_id})


class CreateTrainingConditionSuccessView(LoginRequiredMixin,  TemplateView):

    template_name = "vrct_app/_trainings/create_training_condition_success.html"

    def get_context_data(self, **kwargs):
        context = super(CreateTrainingConditionSuccessView, self).get_context_data(**kwargs)
        context['condition'] = Condition.objects.get(pk=self.kwargs['condition_id'])

        return context


# CREATE TRAINING - Training Experimental Group
class CreateTrainingGroupView(LoginRequiredMixin, FormView):

    template_name = "vrct_app/_trainings/create_training_group.html"
    form_class = GroupForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.instance.created_by = self.request.user
        new_group = form.save()
        self.group_id = new_group.pk
        return super(CreateTrainingGroupView, self).form_valid(form)

    def get_success_url(self):
         return reverse_lazy('create_training_group_success', kwargs={'group_id': self.group_id})


class CreateTrainingGroupSuccessView(LoginRequiredMixin,  TemplateView):

    template_name = "vrct_app/_trainings/create_training_group_success.html"

    def get_context_data(self, **kwargs):
        context = super(CreateTrainingGroupSuccessView, self).get_context_data(**kwargs)
        context['group'] = ExperimentalGroup.objects.get(pk=self.kwargs['group_id'])

        return context
