from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import FormView, TemplateView, ListView
from django.urls import reverse_lazy

from django.contrib.auth.models import User
from vrct_app.forms import UserForm

# -------
# MANAGE USERS
# -------

# VIEW USERS
class UserList(LoginRequiredMixin, ListView):

    template_name = "vrct_app/_user/user_list.html"

    # show only users who are in group Administartor or Operator
    queryset = User.objects.filter(groups__name__in=['Administrator', 'Training Operator']).order_by('pk')
    context_object_name = 'user_list'
    paginate_by = 25


# Create new User
class CreateUserView(LoginRequiredMixin, FormView):

    template_name = "vrct_app/_user/create_user.html"
    form_class = UserForm

    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.instance.created_by = self.request.user
        new_user = form.save()
        group_name = form.cleaned_data['groups'].values_list('name', flat=True).first()
        group_pk = form.cleaned_data['groups'].values_list('pk', flat=True).first()
        new_user.groups.add(group_pk)

        if group_name == 'Administrator':
            new_user.is_staff = True
            new_user.is_superuser = True
            new_user.save()

        return super(CreateUserView, self).form_valid(form)

    def get_success_url(self):
         return reverse_lazy('user_list')