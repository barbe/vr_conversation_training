# operator-app (operator-app)

Operator App for the VR Conversation Training

## Install quasar

```bash
npm i -g @quasar/cli
```

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in dev mode ELECTRON

```bash
quasar dev -m electron
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build -m electron
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-webpack/quasar-config-js).
