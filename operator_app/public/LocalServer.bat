cd "C:\Users\Peter Fromberger\Desktop\vrct_conversation_engine_ws-main\BINARIES"

set "host=%1"
set "port=%2"
set "room=%3"
set "bot_variant=%4"
set "ws=ws://%host%:%port%/ws/chat/%room%/"

ChatScript local debug=':build vrct %bot_variant%' & Chatscript websocket=%ws% userlog serverlog

