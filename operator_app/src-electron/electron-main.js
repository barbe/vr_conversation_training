import { app, BrowserWindow, ipcMain, nativeTheme } from "electron";
import { initialize, enable } from "@electron/remote/main";
import path from "path";
import os from "os";

import devtools from "@vue/devtools";
import * as childProcess from "child_process";
import ps from "ps-node";

initialize();

// needed in case process is undefined under Linux
const platform = process.platform || os.platform();

/**
 * Start bash-script to handle chatscript
 */

const execFile = childProcess.execFile;

async function handleStartChatscript(arg1, data) {
  console.log("electron-main.js --> startChatscript()");

  const publicFolder = path.resolve(
    __dirname,
    process.env.QUASAR_PUBLIC_FOLDER
  );

  const script = path.join(publicFolder, "LocalServer.bat");

  // that works!
  const cs_process = execFile(script, [
    data.ws_host,
    data.ws_port,
    data.ws_room,
    data.bot_variant,
  ]);

  console.log("CS started --> now send the start command for bot...");

  cs_process.stdout.on("data", (data) => {
    console.log("stdout: " + data);
  });

  cs_process.stderr.on("data", (err) => {
    console.log("stderr: " + err);
  });

  cs_process.on("exit", (code) => {
    console.log("exit: " + code);
  });
}

// A simple pid lookup
function check_and_stop_cs_server() {
  ps.lookup({ command: "chatscript" }, function (err, resultList) {
    if (err) {
      throw new Error(err);
    }

    resultList.forEach(function (process) {
      // chatscript is already running
      if (process) {
        var error_msg = "chatscript is already running! PID: %s" + process.pid;
        console.log(error_msg);

        ps.kill(process.pid, function (err) {
          if (err) {
            throw new Error(err);
          }
        });

        // chatscript is not running
      } else {
        console.log("chatscript is not running!");
      }
    });
  });
}

try {
  if (platform === "win32" && nativeTheme.shouldUseDarkColors === true) {
    require("fs").unlinkSync(
      path.join(app.getPath("userData"), "DevTools Extensions")
    );
  }
} catch (_) {}

let mainWindow;

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    icon: path.resolve(__dirname, "icons/icon.png"), // tray icon
    width: 1920,
    height: 1200,
    useContentSize: true,
    frame: false,
    webPreferences: {
      contextIsolation: true,
      // More info: /quasar-cli/developing-electron-apps/electron-preload-script
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD),
    },
  });

  enable(mainWindow.webContents);

  mainWindow.loadURL(process.env.APP_URL);

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    mainWindow.webContents.openDevTools();
    devtools.connect(/* host, port */);
  } else {
    // we're on production; no access to devtools pls
    mainWindow.webContents.on("devtools-opened", () => {
      mainWindow.webContents.closeDevTools();
    });
  }

  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

app.whenReady().then(async () => {
  ipcMain.handle("chatscript:startChatscript", handleStartChatscript);
  ipcMain.handle(
    "chatscript:check_and_stop_cs_server",
    check_and_stop_cs_server
  );
  createWindow();
});

app.on("window-all-closed", () => {
  if (platform !== "darwin") {
    // kill all running processes
    check_and_stop_cs_server();
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
