import { api } from "boot/axios";
import { useAuthStore } from "stores/auth";

const LoginRoutine = (user) =>
  new Promise((resolve, reject) => {
    /**
     * Summary. Basic authentication. Tries to get a Token
     *
     * @param username      The username which is used for authentification
     * @param password  	The password which is used for authentification.
     */

    const auth = useAuthStore();

    const payload =
      "username=" + user.username.value + "&password=" + user.password.value;
    const endpoint = "api/api-token-auth/";

    api
      .post(endpoint, payload)

      .then((resp) => {
        if (resp.data.error) {
          // remove the axios default header
          delete api.defaults.headers.common["Authorization"];
        } else {
          auth.changeIsAuthorized(true);
          auth.changeToken(resp.data.token);
          auth.changeUsername(resp.data.username);
          auth.changeUserId(resp.data.user_id);
          auth.changeUserGroup(resp.data.usergroup);

          // set header for axios
          api.defaults.headers.common["Authorization"] =
            "Token " + resp.data.token;
        }

        resolve(resp);
      })

      .catch((err) => {
        // remove the axios default header
        delete api.defaults.headers.common["Authorization"];

        auth.changeIsAuthorized(false);
        auth.changeToken("");
        auth.changeUsername("");
        auth.changeUserId(null);

        reject(err);
      });
  });

const LogoutRoutine = () =>
  new Promise((resolve, reject) => {
    /**
     * Summary. Basic authentication. Tries to get a Token
     *
     * @param username      The username which is used for authentification
     * @param password  	The password which is used for authentification.
     */

    const auth = useAuthStore();

    try {
      // remove the axios default header
      delete api.defaults.headers.common["Authorization"];

      // clean up store
      auth.changeIsAuthorized(false);
      auth.changeToken("");
      auth.changeUsername("");
      auth.changeUserId(null);
    } finally {
      resolve();
    }
  });

export default {
  LoginRoutine,
  LogoutRoutine,
};
