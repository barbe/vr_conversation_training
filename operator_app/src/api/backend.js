import { api } from "boot/axios";
import { useRunningTrainingStore } from "stores/running_training";

const GetCurrentTrainings = (user) =>
  new Promise((resolve, reject) => {
    const endpoint = "api/current-trainings-list/";

    // use store
    const training_store = useRunningTrainingStore();

    api
      .get(endpoint)

      .then((resp) => {
        training_store.changeTrainingList(resp.data.Items);

        resolve(resp);
      })

      .catch((err) => {
        reject(err);
      });
  });

const UpdateTrainingRunning = (value) =>
  new Promise((resolve, reject) => {
    const endpoint = "api/update-training-running/";

    const training_store = useRunningTrainingStore();

    var formData = {
      pk: training_store.getCurrentTraining.id,
      value: value,
    };

    api
      .put(endpoint, formData)

      .then((resp) => {
        resolve(resp);
      })

      .catch((err) => {
        error_handling(err);
      });
  });

const UpdateTrainingFinished = (value) =>
  new Promise((resolve, reject) => {
    const endpoint = "api/update-training-finished/";

    const training_store = useRunningTrainingStore();

    var formData = {
      pk: training_store.getCurrentTraining.id,
      value: value,
    };

    api
      .put(endpoint, formData)

      .then((resp) => {
        resolve(resp);
      })

      .catch((err) => {
        error_handling(err);
      });
  });

const SaveTrialData = () =>
  new Promise((resolve, reject) => {
    const endpoint = "api/save-trial-data/";

    const training_store = useRunningTrainingStore();

    var basic_data = {
      trainee: training_store.trialDataBasicInfo.trainee,
      training: training_store.trialDataBasicInfo.training,
      condition: training_store.trialDataBasicInfo.condition,
      bot: training_store.trialDataBasicInfo.bot,
      bot_variant: training_store.trialDataBasicInfo.bot_variant,
    };

    var data = {
      trial_nr: training_store.trialNr,
      trial_start_time: training_store.trialStartTime,
      trial_end_time: training_store.trialEndTime,
      trial_duration: training_store.trialDuration,
      question_content: training_store.trialQuestion,
      answer_content: training_store.trialAnswer,
    };

    // cs_variables
    for (const [key, value] of Object.entries(
      training_store.trialDataCsVariables
    )) {
      data[key] = value;
    }

    // operator_variables
    for (const [key, value] of Object.entries(
      training_store.trialDataOperatorVariables
    )) {
      data[key] = value;
    }

    console.log("trialData:");
    console.log(data);

    var formData = {
      data: data,
      basic_data: basic_data,
    };

    api
      .post(endpoint, formData)

      .then((resp) => {
        resolve(resp);
      })

      .catch((err) => {
        error_handling(err);
      });
  });

const DeleteTrainingData = (value) =>
  new Promise((resolve, reject) => {
    const endpoint = "api/delete-training-data/";

    const training_store = useRunningTrainingStore();

    var formData = {
      trainee: training_store.trialDataBasicInfo.trainee,
      training: training_store.trialDataBasicInfo.training,
      condition: training_store.trialDataBasicInfo.condition,
      bot: training_store.trialDataBasicInfo.bot,
      bot_variant: training_store.trialDataBasicInfo.bot_variant,
    };

    api
      .post(endpoint, formData)

      .then((resp) => {
        resolve(resp);
      })

      .catch((err) => {
        error_handling(err);
      });
  });

export default {
  GetCurrentTrainings,
  UpdateTrainingRunning,
  UpdateTrainingFinished,
  SaveTrialData,
  DeleteTrainingData,
};
