import { boot } from 'quasar/wrappers'
import { useAuthStore } from 'stores/auth'

export default boot(({ router, store }) => {

  router.beforeEach((to, from, next) => {
    // Now you need to add your authentication logic here, like calling an API endpoint

    const auth = useAuthStore()

    console.log('isAuthorized: ' + auth.isAuthorized)
    if (to.matched.some(record => record.meta.requireAuth) && !auth.isAuthorized) {
      next({ name: 'login', query: { next: to.fullPath } })
    } else {
      next()
    }
  })

})