import { useRunningTrainingStore } from "stores/running_training";
const trainingStore = useRunningTrainingStore();

var trainingTimer_id = null;
var trainingStartTime = null;

// training timer
function training_timer_start() {
  // set start time
  trainingStartTime = Date.now();

  // start timer
  trainingTimer_id = setInterval(() => {
    createNewTimestamp();
  }, 1000);
}

function createNewTimestamp() {
  var data = Date.now() - trainingStartTime;
  trainingStore.changeTrainingTime(data);
}

function training_timer_stop() {
  clearInterval(trainingTimer_id);
}

export default {
  training_timer_start,
  training_timer_stop,
};
