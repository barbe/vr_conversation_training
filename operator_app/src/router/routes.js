const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/NotDev.vue"),
        meta: { requireAuth: true },
      },
      {
        path: "/login",
        name: "login",
        component: () => import("pages/LoginPage.vue"),
      },
      {
        path: "/dev",
        name: "dev",
        component: () => import("pages/DevChoose.vue"),
        meta: { requireAuth: true },
      },
      {
        path: "/devmode",
        name: "devmode",
        component: () => import("pages/DevMode.vue"),
        meta: { requireAuth: true },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
