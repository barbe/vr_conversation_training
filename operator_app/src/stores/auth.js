import { defineStore } from "pinia";

export const useAuthStore = defineStore("auth", {
  state: () => ({
    is_authorized: false,
    token: "",
    username: "",
    user_id: null,
    user_group: "",
  }),

  getters: {
    isAuthorized: (state) => state.is_authorized,
    getToken: (state) => state.token,
    getUsername: (state) => state.username,
    getUserId: (state) => state.user_id,
    getUserGroup: (state) => state.user_group,
  },

  actions: {
    changeIsAuthorized(data) {
      this.is_authorized = data;
    },
    changeUsername(data) {
      this.username = data;
    },
    changeToken(data) {
      this.token = data;
    },
    changeUserId(data) {
      this.user_id = data;
    },
    changeUserGroup(data) {
      this.user_group = data;
    },
  },
});
