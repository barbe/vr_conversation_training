import { defineStore, acceptHMRUpdate } from "pinia";

export const useRunningTrainingStore = defineStore("runningTraining", {
  state: () => ({
    // not dev process
    step_ok: false,
    training_scene_started: false,
    training_finished: false,
    training_time: "00:00:00",
    status_message: "Waiting for start...",

    // trainings
    training_list: [],
    running_training_api: {},

    // websocket client
    ws_host: "127.0.0.1",
    ws_port: "8000",

    ws_open: false,

    // messages
    messages: [],
    filtered_messages: [],
    last_message_type: "",
    last_message_from: "",
    last_message_to: "",
    last_message_text: "",

    last_question_unity_to_app: {},
    last_question_app_to_cs: {},
    last_answer: {},

    // chatscript
    cs_connected: false,
    cs_heartbeat: false,

    // unity
    unity_connected: false,
    unity_heartbeat: false,

    // trial data
    trial_nr: 0,
    trial_start_time: "",
    trial_end_time: "",
    trial_duration: null,
    trial_question: null,
    trial_answer: null,
    trial_data_basicInfo: {},
    trial_data_cs_variables: {},
    trial_data_operator_variables: {},
  }),

  getters: {
    getCurrentTraining: (state) => state.running_training_api,

    // websocket
    wsPort: (state) => state.ws_port,
    wsHost: (state) => state.ws_host,
    wsRoom: (state) => state.running_training_api.ws_room,
    wsBotVariant: (state) => state.running_training_api.bot_variant,
    wsOpen: (state) => state.ws_open,
    botVariables: (state) => state.running_training_api.bot_variables,
    operatorVariables: (state) => state.running_training_api.operator_variables,

    // process variables
    stepOk: (state) => state.step_ok, // stepper continue enabled/disabled
    trainingSceneStarted: (state) => state.training_scene_started, // training scene in unity started -> training really started
    trainingTime: (state) => state.training_time,
    statusMessage: (state) => state.status_message,
    trainingFinished: (state) => state.training_finished,

    // last message
    lastMessageType: (state) => state.last_message_type,
    lastMessageFrom: (state) => state.last_message_from,
    lastMessageTo: (state) => state.last_message_to,
    lastMessageText: (state) => state.last_message_text,
    lastQuestionUnityToApp: (state) => state.last_question_unity_to_app,
    lastQuestionAppToCs: (state) => state.last_question_app_to_cs,
    lastAnswer: (state) => state.last_answer,

    // messages
    allMessages: (state) => state.messages,
    filteredMessages: (state) => state.filtered_messages,
    //newQuestion: (state) => state.new_question,

    // chatscript
    csConnected: (state) => state.cs_connected,
    csHeartbeat: (state) => state.cs_heartbeat,

    // unity
    unityConnected: (state) => state.unity_connected,
    unityHeartbeat: (state) => state.unity_heartbeat,
    unityScene: (state) => state.running_training_api.unity_scene,

    // trial data
    trialNr: (state) => state.trial_nr,
    trialStartTime: (state) => state.trial_start_time,
    trialEndTime: (state) => state.trial_end_time,
    trialDuration: (state) => state.trial_duration,
    trialDataBasicInfo: (state) => state.trial_data_basicInfo,
    trialQuestion: (state) => state.trial_question,
    trialAnswer: (state) => state.trial_answer,
    trialDataCsVariables: (state) => state.trial_data_cs_variables,
    trialDataOperatorVariables: (state) => state.trial_data_operator_variables,
  },

  actions: {
    changeTrainingList(data) {
      this.training_list = data;
    },

    changeStepOk(data) {
      this.step_ok = data;
    },

    changeTrainingSceneStarted(data) {
      this.training_scene_started = data;
    },

    changeTrainingFinished(data) {
      this.training_finished = data;
    },

    changeRunningTrainingApi(data) {
      this.running_training_api = data;

      // define TrialDataBasicInfo
      if (Object.keys(data).length === 0) {
        this.trial_data_basicInfo = {};
      } else {
        this.trial_data_basicInfo = {
          trainee: data.trainee,
          training: data.training,
          condition: data.condition,
          bot: data.bot,
          bot_variant: data.bot_variant,
        };
      }
    },

    changeTrainingTime(data) {
      var min_str;
      var sec_str;
      var hrs_str;

      // 1 - Convert to seconds:
      var seconds = Math.round(data / 1000);

      // 2 - Extract minutes:
      var minutes = parseInt(seconds / 60); // 60 seconds in 1 minute

      // 3 - Extract hours:
      var hours = parseInt(minutes / 60); // 60 seconds in 1 minute

      // 4 - Keep only seconds not extracted to minutes:
      seconds = seconds % 60;
      minutes = minutes % 60;

      if (seconds < 10) {
        sec_str = "0" + seconds;
      } else {
        sec_str = seconds;
      }

      if (minutes < 10) {
        min_str = "0" + minutes;
      } else {
        min_str = minutes;
      }

      if (hours < 10) {
        hrs_str = "0" + hours;
      } else {
        hrs_str = hours;
      }

      // 4 - formatted
      this.training_time = hrs_str + ":" + min_str + ":" + sec_str;
    },

    changeStatusMessage(data) {
      this.status_message = data;
    },

    // websockets
    changeWsHost(data) {
      this.ws_host = data;
    },
    changeWsPort(data) {
      this.ws_port = data;
    },

    changeWsOpen(data) {
      this.ws_open = data;
    },

    // messages
    addMessage(data) {
      // add to array containing all messages
      this.messages.push(data);

      // change last_message_type
      this.last_message_type = data.type;
      this.last_message_from = data.from;
      this.last_message_to = data.to;
      this.last_message_text = data.text;

      // if new message is an answer from cs
      if (
        (data.type == "answer") &
        (data.from == "cs") &
        (data.to == "unity")
      ) {
        this.last_answer = data;

        // ToDo: SAVE DATA!!!
      }

      // if new message is a question from trainee (unity -> app)
      if (
        (data.type == "question") &
        (data.from == "unity") &
        (data.to == "app")
      ) {
        this.last_question_unity_to_app = data;
      }

      // if new message is a question from trainee already categorized by operator (app -> cs)
      // this is the relevant info for backend to save data
      if (
        (data.type == "question") &
        (data.from == "app") &
        (data.to == "cs")
      ) {
        this.last_question_app_to_cs = data;
      }
    },

    addFilteredMessage(data) {
      // we add a new message at the beginning of the array
      // in order have the last message always on top
      this.filtered_messages.unshift(data);
    },

    changeNewQuestion(data) {
      this.new_question = data;
    },

    // chatscript
    changeCsConnected(data) {
      this.cs_connected = data;
    },
    changeCsHeartbeat(data) {
      this.cs_heartbeat = data;
    },

    // unity
    changeUnityConnected(data) {
      this.unity_connected = data;
    },
    changeUnityHeartbeat(data) {
      this.unity_heartbeat = data;
    },

    // trialData
    changeTrialNr(data) {
      this.trial_nr = data;
    },
    changeTrialStartTime(data) {
      this.trial_start_time = data;
    },
    changeTrialEndTime(data) {
      this.trial_end_time = data;
    },
    changeTrialDuration(data) {
      this.trial_duration = data;
    },
    changeTrialDataBasicInfo(data) {
      this.trial_data_basicInfo = data;
    },
    changeTrialQuestion(data) {
      this.trial_question = data;
    },
    changeTrialAnswer(data) {
      this.trial_answer = data;
    },
    changeTrialDataCsVariables(data) {
      this.trial_data_cs_variables = data;
    },
    changeTrialDataOperatorVariables(data) {
      this.trial_data_operator_variables = data;
    },
    resetTrialData(restart) {
      this.trial_nr = 0;
      this.trial_start_time = "";
      this.trial_end_time = "";
      this.trial_duration = null;
      this.trial_question = null;
      this.trial_answer = null;
      this.trial_data_cs_variables = {};
      this.trial_data_operator_variables = {};

      this.messages = [];
      this.filtered_messages = [];
      this.last_message_type = "";
      this.last_message_from = "";
      this.last_message_to = "";
      this.last_message_text = "";

      this.last_question_unity_to_app = {};
      this.last_question_app_to_cs = {};
      this.last_answer = {};

      if (!restart) {
        this.running_training_api = {};
        this.trial_data_basicInfo = {};
      }
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useMainStore, import.meta.hot));
}
