import { useRunningTrainingStore } from "stores/running_training";
import heartbeat from "./heartbeat.js";
import backend from "../api/backend";

const trainingStore = useRunningTrainingStore();

var connection = null;

// start ws-client
function startWsClient() {
  const ws_path =
    "ws://" +
    trainingStore.wsHost +
    ":" +
    trainingStore.wsPort +
    "/ws/chat/" +
    trainingStore.wsRoom +
    "/";

  // create new Websocket-client
  connection = new WebSocket(ws_path);

  connection.onmessage = (event) => {
    // messages are in JSON
    parseMessage(event.data);
  };

  connection.onopen = (event) => {
    trainingStore.changeWsOpen(true);

    // start heartbeat function for unity
    heartbeat.heartbeatUnityStartIntervall();

    // start heartbeat function for cs
    // here we need to send every two seconds a heartbeat message to cs and cs answers with a heartbeat message
    heartbeat.heartbeatCsSendStartIntervall();
    heartbeat.heartbeatCsStartIntervall();
  };

  connection.onclose = (event) => {
    trainingStore.changeWsOpen(false);
  };
}

function closeWsClient() {
  if (trainingStore.ws_open) {
    connection.close();

    // stop heartbeat functions
    heartbeat.heartbeatUnityClearIntervall();
    heartbeat.heartbeatCsClearIntervall();
  }
}

// send message to websocket-server
function sendMessage(from, to, msg, type) {
  if (trainingStore.ws_open) {
    var message = JSON.stringify([
      {
        text: msg,
        from: from,
        to: to,
        type: type,
      },
    ]);

    connection.send(message);
  }
}

// only for dev!!!
// send simulated cs answer to unity
function sendCsSimulatedAnswer(soundFile, actionType) {
  if (trainingStore.ws_open) {
    var message = JSON.stringify([
      {
        text: "Hallo, ich bin Tim.",
        from: "cs",
        to: "unity",
        type: "answer",
        soundFile: soundFile,
        actionType: actionType,
        topic: "introduction",
        answerCategory: "ignorance",
        questionCounter: 2,
        randNum: 51,
        probFormat: "80_20",
      },
    ]);

    connection.send(message);
  }
}

// parse the ws-message
function parseMessage(msg) {
  // message is in JSON
  var data = JSON.parse(msg);

  // and the first item in the list
  var message = data[0];

  // add the message to the main message store
  trainingStore.addMessage(message);

  handleWsMessage(message);
}

// handle the ws-message
function handleWsMessage(message) {
  // question from unity
  if (
    message.type == "question" &&
    message.from == "unity" &&
    message.to == "app"
  ) {
    // we add the message to the filtered array
    trainingStore.addFilteredMessage(message);

    // we save the question in trialData and change the trialNr
    trainingStore.changeTrialNr(trainingStore.trialNr + 1);

    // we save the timestamp in trialData
    const dt = new Date(message.timestamp);
    trainingStore.changeTrialStartTime(dt);

    // we save the question in trialData
    trainingStore.changeTrialQuestion(message.text);
  }

  // answer from cs
  if (
    message.type == "answer" &&
    message.from == "cs" &&
    message.to == "unity"
  ) {
    // we add the message to the filtered array
    trainingStore.addFilteredMessage(message);

    // we save the answer in trialData
    trainingStore.changeTrialAnswer(message.text);

    // we save the timestamp in trialData
    const dt = new Date(message.timestamp);
    trainingStore.changeTrialEndTime(dt);

    // calculate and change trialDuration
    const dur = trainingStore.trialEndTime - trainingStore.trialStartTime;
    trainingStore.changeTrialDuration(dur);

    // we save the cs_variables in trialData
    var data = {};
    for (const [key, value] of Object.entries(message)) {
      console.log(key + ": " + value);
      if (
        key != "type" &&
        key != "from" &&
        key != "to" &&
        key != "text" &&
        key != "timestamp" &&
        key != "duration"
      ) {
        data["cs_" + key] = value;
      }
    }

    console.log(data);

    trainingStore.changeTrialDataCsVariables(data);

    // ToDo: save trial data in backend
    backend.SaveTrialData().then((resp) => {
      // ToDo: reset trialData!
      console.log("trial data send to backend...");
    });
  }

  // unity system messages
  if (
    message.from == "unity" &&
    message.to == "app" &&
    message.type == "system"
  ) {
    // training scene is loaded and trainee can start to train
    if (message.text == "training scene started") {
      trainingStore.changeTrainingSceneStarted(true);
    }
  }

  if (
    message.from == "unity" &&
    message.to == "app" &&
    message.type == "system"
  ) {
    // training scene is stopped
    if (message.text == "training scene stopped") {
      trainingStore.changeTrainingSceneStarted(false);
    }
  }

  if (
    message.from == "unity" &&
    message.to == "app" &&
    message.type == "system"
  ) {
    // training scene is stopped
    if (message.text == "training finished") {
      trainingStore.changeTrainingFinished(true);
    }
  }

  // heartbeat messages
  if (
    message.from == "unity" &&
    message.to == "app" &&
    message.type == "heartbeat"
  ) {
    // heartbeat from unity
    if (message.text == "heartbeat") {
      trainingStore.changeUnityHeartbeat(true);
    }
  }

  if (
    message.from == "cs" &&
    message.to == "app" &&
    message.type == "heartbeat"
  ) {
    // heartbeat from unity
    if (message.text == "heartbeat") {
      trainingStore.changeCsHeartbeat(true);
    }
  }

  // messages for unity
  if (message.to == "unity") {
    //console.log("Message for unity: " + message.text);
    // messages for chatscript
  } else if (message.to == "cs") {
    //console.log("Message for chatscript: " + message.text);
    // message for backend
  } else if (message.to == "backend") {
    //console.log("Message for backend: " + message.text);
    // message for app
  } else if (message.to == "app") {
    //console.log("Message for app: " + message.text);
    // else
  } else {
    console.log("Message has no valid 'to'-value!");
  }
}

export default {
  startWsClient,
  closeWsClient,
  sendMessage,
  sendCsSimulatedAnswer,
};
