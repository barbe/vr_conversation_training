import { useRunningTrainingStore } from "stores/running_training";
import ws_client from "./client";

const trainingStore = useRunningTrainingStore();

var unityIntervall_id = null;
var csIntervall_id = null;
var csSendIntervall_id = null;

function heartbeatUnityStartIntervall() {
  unityIntervall_id = setInterval(clear_interval, 2000);

  function clear_interval() {
    //console.log("heartbeatUnity: clear_interval");
    if (trainingStore.unityHeartbeat) {
      trainingStore.changeUnityHeartbeat(false);
      trainingStore.changeUnityConnected(true);
    } else {
      trainingStore.changeUnityConnected(false);
    }
  }
}

function heartbeatUnityClearIntervall() {
  clearInterval(unityIntervall_id);
}

function heartbeatCsStartIntervall() {
  csIntervall_id = setInterval(clear_interval, 2000);

  function clear_interval() {
    //console.log("heartbeatCs: clear_interval");
    if (trainingStore.csHeartbeat) {
      trainingStore.changeCsHeartbeat(false);
      trainingStore.changeCsConnected(true);
    } else {
      trainingStore.changeCsConnected(false);
    }
  }
}

function heartbeatCsClearIntervall() {
  clearInterval(csIntervall_id);
}

function heartbeatCsSendStartIntervall() {
  csSendIntervall_id = setInterval(clear_interval, 2000);

  function clear_interval() {
    // send message to heartbeat message to cs
    ws_client.sendMessage("app", "cs", "heartbeat", "heartbeat");
  }
}

function heartbeatCsSendClearIntervall() {
  clearInterval(csSendIntervall_id);
}

export default {
  heartbeatUnityStartIntervall,
  heartbeatUnityClearIntervall,
  heartbeatCsStartIntervall,
  heartbeatCsClearIntervall,
  heartbeatCsSendStartIntervall,
  heartbeatCsSendClearIntervall,
};
