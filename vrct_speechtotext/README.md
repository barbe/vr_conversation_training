# VRCT speech-to-text

To allow users to ask questions freely, we used a speech to text module in our framework. To stay in line with the open-source idea, we chose the Kaldi speech recognition toolkit (https://kaldi-asr.org/). It allows you to integrate different speech models and can be run as a server in the background. For our framework we have to set up Kaldi on a virtual Ubuntu machine running on the same computer. The virtual machine communicates with the virtual environment (Unity) via a websocket connection (Url for Unity: http://192.168.119.162:8080/client/dynamic/recognize -> can be customized in script). The Kaldi server receives the audio files to be transcribed and sends back a transcript in text form. Since the image of the virtual machine is too big to upload, it has to be set up independently. We used the Gstreamer framework to set up the Kaldi server with two workers. The instructions for the setup can be found here (https://github.com/alumae/kaldi-gstreamer-server). 

If you have any questions or need help, feel free to contact us:
 
hermann.barbe@med.uni-goettingen.de

peter.fromberger@med.uni-goettingen.de