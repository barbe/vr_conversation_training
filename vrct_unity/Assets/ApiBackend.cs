using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

public class ApiBackend : MonoBehaviour
{
    
    public string apiHost = "127.0.0.1";
    public string apiPort = "8000";
    private string baseURL;
    
    [Serializable]
    public class ApiUser
    {
        public string token;
        public string user_id;
        public string username;
    }

    public ApiUser User = new ApiUser();

    // from: https://stackoverflow.com/questions/36239705/serialize-and-deserialize-json-and-json-array-in-unity
    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }

    [Serializable]
    public class Training
    {
        public string training;
        public string trainee;
        public string condition;
        public string bot;
        public string bot_variant;
        public string ws_room;
        public string unity_scene;
    }
    
    // Start is called before the first frame update
    void Start()
    {

        baseURL = "http://" + apiHost + ":" + apiPort + "/";

        StartCoroutine(Login());
    }

    IEnumerator Login()
    {
        WWWForm form = new WWWForm();

        // hardcoded login data!
        form.AddField("username", "vrct");
        form.AddField("password", "vrct");

        string endpoint = "api/api-token-auth/";

        using (UnityWebRequest www = UnityWebRequest.Post(baseURL + endpoint, form))
        {            
            Debug.Log(www);

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Succesfully logged into backend!");

                // Show results as text
                User = JsonUtility.FromJson<ApiUser>(www.downloadHandler.text);
                StartCoroutine(GetCurrentRunningExperiments(User));

            }
        }

    }

    IEnumerator GetCurrentRunningExperiments(ApiUser Token) {

        string endpoint = "api/current-trainings-list/";

        using (UnityWebRequest www = UnityWebRequest.Get(baseURL + endpoint))
        {

            www.SetRequestHeader("Authorization", "Token " + Token.token);

            yield return www.SendWebRequest();
 
            if(www.result != UnityWebRequest.Result.Success) {
                Debug.Log(www.error);
            }
            else {

                // Show results as text
                Training[] trainingList = JsonHelper.FromJson<Training>(www.downloadHandler.text);
    
            }
        }
        
    }
}
