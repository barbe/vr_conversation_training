using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevScript : MonoBehaviour
{

    public string wsMessage;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void send() {
        // send simple question
        GameObject controller = GameObject.Find("WebsocketManager");
        WebsocketManager websocketManager = controller.GetComponent<WebsocketManager>();
        websocketManager.SendMessage("unity", "app", "question", wsMessage);
    }
}
