using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(DevScript))]
public class DevScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DevScript myScript = (DevScript)target;
        if(GUILayout.Button("Send"))
        {
            myScript.send();
        }
    }
}
