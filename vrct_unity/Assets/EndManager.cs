using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class EndManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        GameObject controller = GameObject.Find("WebsocketManager");
        WebsocketManager websocketManager = controller.GetComponent<WebsocketManager>();

        websocketManager.SendMessage("unity", "app", "system", "training finished");

        // close ws
        websocketManager.CloseWebsocket();

        // ToDo: reset TrainingManager

        // ToDo UI which allows to come to StartScene

    }


}
