﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionController : MonoBehaviour
{
    private static ActionController playerInstance;
    AudioSource source;
    Animator anim;

    public bool canTalk = true;

    void Awake()
    {

        // Subscribe to TutorialTrainingManagerEvents
        TutorialTrainingManager.VoiceMessageEvent += PlayFileDispatch;
        TutorialTrainingManager.ActionStateEvent += ChangeActionState;
        
        
        
       // Debug.Log("subscribed to ActionStateEvent");
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();


    }

    void OnDestroy() {

        // Unsubscribe from TutorialTrainingManagerEvents. Important if training session has to be restarted!
        TutorialTrainingManager.VoiceMessageEvent -= PlayFileDispatch;
        TutorialTrainingManager.ActionStateEvent -= ChangeActionState;
    }
    void Update()
    {
        if (source.isPlaying)
        {
            anim.SetBool("is_talking", true);
        }

        else
        {
            anim.SetBool("is_talking", false);
        }
    }


        
    void ChangeActionState(string action)
    {
        //change characters action depending on state send from app


        //  Debug.Log("changing action state");
        
        if(action.Contains("communicative"))
        {
        //  Debug.Log("changing to communicative");
            anim.SetTrigger("state_communicative");
            canTalk = false;
            StartCoroutine(CheckTransition(action));
        //  Debug.Log("coroutine started");
        }

        else if (action.Contains("reserved"))
        {
        //  Debug.Log("changing to reserved");
            anim.SetTrigger("state_reserved");
            canTalk = false;
            StartCoroutine(CheckTransition(action));
        //  Debug.Log("coroutine started");
        }
    }

    IEnumerator CheckTransition (string action)
    {
        // wait until animation has finished so that new talking animation can be initiated

        //  Debug.Log("checking transition to" + action);
        //  Debug.Log("currentanimatorstateinfo" + anim.GetCurrentAnimatorStateInfo(0));
        yield return new WaitUntil(() => anim.GetCurrentAnimatorStateInfo(0).IsName(action));
        //  Debug.Log("checked transition to" + action);
        canTalk = true;
    }

    void PlayFileDispatch(string character, string msg)
    {
        // start playing corresponding audio file containing charakters answer, when character 'canTalk' again
        StartCoroutine(WaitTillPlay(character, msg));
    }




    IEnumerator WaitTillPlay(string charname, string filename)
    {
        yield return new WaitForSeconds(0.2f);
        yield return new WaitUntil(() => canTalk);
        StartCoroutine(PlayFile(charname, filename));
    }
    

    IEnumerator PlayFile(string charname, string filename)
    {

        // function to look for corresponding audiofile in the 'Audiofiles/charactername' folder. Files have to be in sync with the soundfile nomenclature in ChatScript for the specific charakter.


        char[] delimiterCharsX = { '-' };
        string[] filearray = filename.Split(delimiterCharsX);
        Debug.Log("Try playing file ");
        //yield return new WaitWhile(() => !canTalk);

        // if only one answer is given play one audiofile
        if (filearray.Length == 1)
        {
            string location = "Audiofiles/" + charname + "/" + filearray[0];
         //Debug.Log("looking for audio clip at " + location);
            AudioClip clip = Resources.Load<AudioClip>(location);
            source.clip = clip;
            source.PlayOneShot(source.clip);
        }

        // with two answers, play the first, wait one second, then play next. 
        if (filearray.Length == 2)
        {
            string location = "Audiofiles/" + charname + "/" + filearray[0];
            Debug.Log("looking for first audio clip at " + location);
            AudioClip clip = Resources.Load<AudioClip>(location);
            //yield return WaitForActionChange();
            source.clip = clip;
            source.PlayOneShot(source.clip);
            yield return new WaitWhile(() => source.isPlaying);
            yield return new WaitForSeconds(1);
            string location2 = "Audiofiles/" + charname + "/" + filearray[1];
            Debug.Log("looking for second audio clip at " + location2);
            clip = Resources.Load<AudioClip>(location2);
            source.clip = clip;
            source.PlayOneShot(source.clip);

        }

        yield return null;
    }


}
