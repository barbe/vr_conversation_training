using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Text;

public class KaldiClient : MonoBehaviour
{

    public string url;

    [Serializable]
    public class KaldiHypotheses
    {
        public string utterance;

    }

    [Serializable]
    public class KaldiResponse {
        public int status;
        public string id;
        public KaldiHypotheses[] hypotheses;
    }

    // delegates
    public delegate void KaldiDelegateEvent(string kaldi_output);
    public static event KaldiDelegateEvent KaldiResponseEvent;


    void Start()
    {
        // Subscribe to SavWavEvent
        SavWav.WavCreatedEvent += Upload;

    }


    private void Upload(string filelocation)
    {
        StartCoroutine(SendAndReceive(filelocation));
    }

    void OnDestroy() {

        // Unsubscribe from SavWavEvent. Important if training session has to be restarted!
        SavWav.WavCreatedEvent -= Upload;
    }


    IEnumerator SendAndReceive(string location) {

        byte[] postData = File.ReadAllBytes(@location);
        KaldiResponse thisResponse = new KaldiResponse();


        // UPLOAD WAV-FILE

        using (UnityWebRequest www = UnityWebRequest.Put(url, postData)) {

            www.SetRequestHeader("Content-Type", "audio/*");

            yield return www.SendWebRequest();
 
        // if error during upload
            if(www.result == UnityWebRequest.Result.ProtocolError) {
                Debug.Log(www.error);
             }
        

        // if upload was succesful
        else {

            Debug.Log("...upload complete!");

            // answer is json
            string response = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

            // decode json-answer of kaldi
            thisResponse = JsonUtility.FromJson<KaldiResponse>(response);

            Debug.Log("answer.id: " + thisResponse.id);
            
            Debug.Log("answer.status: " + thisResponse.status);

            // 0 -- Success. Usually used when recognition results are sent
            if (thisResponse.status == 0) {
                Debug.Log("...Success");
                if (thisResponse.hypotheses[0].utterance == "")
                {
                    Debug.Log("Empty response received from kaldi, will not send to App");
                }

                // Send the response to the App 
                else
                {
                    string lowerCaseResponse = thisResponse.hypotheses[0].utterance.ToLower();
                    GameObject controller = GameObject.Find("WebsocketManager");
                    WebsocketManager websocketManager = controller.GetComponent<WebsocketManager>();
                    websocketManager.SendMessage("unity", "app", "question", lowerCaseResponse);
                    KaldiResponseEvent(lowerCaseResponse);
                    
                    Debug.Log("SR-Output = " + lowerCaseResponse);
                }


            // 1 -- No speech. Sent when the incoming audio contains a large portion of silence or non-speech.
            } else if (thisResponse.status == 1) {
                Debug.Log("No speech. Sent when the incoming audio contains a large portion of silence or non-speech.");

            // 2 -- Aborted. Recognition was aborted for some reason.
            } else if (thisResponse.status == 2) {
                Debug.Log("Aborted. Recognition was aborted for some reason.");

        	// 9 -- Not available. Used when all recognizer processes are currently in use and recognition cannot be performed.
            } else if (thisResponse.status == 9) {
                Debug.Log("Aborted. Recognition was aborted for some reason.");
            } else { 
                Debug.Log("Problem mit response.status");
            }
        }
        }


    }
}


















