﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.SceneManagement;
//using System.Threading;

public class RecordAudio : MonoBehaviour
{

    AudioClip mic;
    AudioSource source;
    private bool isPlaying;

    private string state;
    public int trialCount = 0;



    private void Awake() {
        isPlaying = false;

        //Subscribe to TrainingManagerEvent

        TrainingManager.LevelChangeEvent += DecodeRequest;
        foreach (var device in Microphone.devices)
        {
           // Debug.Log("Name: " + device);
        }
    }

    void DecodeRequest()
    {
        mic = Microphone.Start(null, false, 8, 16000);
    }

    // Update is called once per frame
    void Update()
    {
        // depending on input (SteamVR controller or keyboard) start recording of 15 second long audiofile. End recording, when button is released and save audiofile (for upload to Kaldi)

        if (SteamVR_Actions.training.StartRecording.GetStateDown(SteamVR_Input_Sources.Any))
        {


                mic = Microphone.Start(null, false, 15, 16000);
                isPlaying = true;
                //Debug.Log("started long recording");
 
        }

        else if (Input.GetKeyDown("space"))
        {

            mic = Microphone.Start(null, false, 15, 16000);
            isPlaying = true;
            //Debug.Log("started recording");
       

        }

        else if (SteamVR_Actions.training.StartRecording.GetStateUp(SteamVR_Input_Sources.Any)
            && isPlaying)
        {
            Microphone.End(null);
            //Debug.Log("finished recording");
            string filelocation = DateTime.Now.ToString("yyyyMMddTHHmmss") + ".wav";
            SavWav.Save(filelocation, mic);
            isPlaying = false;
        }

        else if (Input.GetKeyUp("space"))
        {
            Microphone.End(null);
            //Debug.Log("finished recording");
            string filelocation = DateTime.Now.ToString("yyyyMMddTHHmmss") + ".wav";
            SavWav.Save(filelocation, mic);
            isPlaying = false;

        }
    }
}
