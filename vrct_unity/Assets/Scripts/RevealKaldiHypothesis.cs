﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


// Is needed to show Kaldi output on TutorialTraining screen.
public class RevealKaldiHypothesis : MonoBehaviour
{
    private TMP_Text m_TextComponent;
    private bool hasTextChanged;

    void Awake()
    {
        m_TextComponent = gameObject.GetComponent<TMP_Text>();

         // Subscribe from KaldiClientEvent.
        KaldiClient.KaldiResponseEvent += Dispatch;
    }

    void OnDestroy()
    {
        // Unsubscribe from KaldiClientEvent. Important if training session has to be restarted!
        KaldiClient.KaldiResponseEvent -= Dispatch;
    }

    void Dispatch(string hyp)
    {
        StartCoroutine(DisplayCurrentHypothesis(hyp));
    }

    IEnumerator DisplayCurrentHypothesis(string hyp)
    {
        m_TextComponent.text = hyp;
        hasTextChanged = true;
        StartCoroutine(RevealWords(m_TextComponent));
        yield return null;
    }

    IEnumerator RevealWords(TMP_Text textComponent)
    {
        textComponent.ForceMeshUpdate();

        int totalWordCount = textComponent.textInfo.wordCount;
        int totalVisibleCharacters = textComponent.textInfo.characterCount; // Get # of Visible Character in text object
        int counter = 0;
        int currentWord = 0;
        int visibleCount = 0;

        while (true)
        {
            currentWord = counter % (totalWordCount + 1);

            // Get last character index for the current word.
            if (currentWord == 0) // Display no words.
                visibleCount = 0;
            else if (currentWord < totalWordCount) // Display all other words with the exception of the last one.
                visibleCount = textComponent.textInfo.wordInfo[currentWord - 1].lastCharacterIndex + 1;
            else if (currentWord == totalWordCount) // Display last word and all remaining characters.
                visibleCount = totalVisibleCharacters;

            textComponent.maxVisibleCharacters = visibleCount; // How many characters should TextMeshPro display?

            // Once the last character has been revealed, wait 1.0 second and start over.
            if (visibleCount >= totalVisibleCharacters)
            {
                break;
            }

            counter += 1;

            yield return new WaitForSeconds(0.1f);

        }
    }

}
