﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCharacter : MonoBehaviour
{
    public GameObject Tim;



    void Start()
    {
        GameObject TrainingManager = GameObject.Find("TrainingManager");

        
        // check charactername of current training session
        var character = TrainingManager.GetComponent<TrainingManager>().currentTraining.bot;

        // hardcoded example of vrct tutorial example. Character prefab and position have to be adjusted for custom scenarios.
        if (character == "vrct")
        {
            Instantiate(Tim, new Vector3(-25.522f, 0.519f, 4.864f), Quaternion.Euler(0, 360, 0));
        }


    }

}
