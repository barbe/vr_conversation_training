using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Valve.VR;

public class TutorialTrainingManager : MonoBehaviour
{
    private static TutorialTrainingManager managerInstance;

  
    [Serializable]
    public class AnswerMessageList
    {
        public AnswerMessage[] message;
    }

    AnswerMessageList answerMessageList;
 
    [Serializable]
    public class AnswerMessage
    {
        public string soundFile;
        public string actionType;
    }

    AnswerMessage answerMessage;

    public delegate void ActionEvent(string action);

    public static event ActionEvent ActionStateEvent;

    public delegate void SpeechEvent(string character, string filepath);
    public static event SpeechEvent VoiceMessageEvent;

    public GameObject DispatcherPrefab;


    public string character = "";

    private string previousAction = "";

    public bool setupChanged = false;




    void Start()
    {
        //get current character name to play the right audio file
        GameObject TrainingManager = GameObject.Find("TrainingManager");
        character = TrainingManager.GetComponent<TrainingManager>().currentTraining.bot;;

        //initialize answermessagelist + answermessage
        answerMessageList = new AnswerMessageList();
        AnswerMessage answerMessage = new AnswerMessage();
        
        //start analyzing answermessage, when websocket triggers a nonsystemmessageevent (meaning this most likely is a message, containing the chatbots answer)
        WebsocketManager.NonSystemMessageReceiveEvent += AnalyzeAnswerMessage;

    }


    

    void OnDestroy()
    {
         // Unsubscribe from WebsocketManager. Important if training session has to be restarted!
        WebsocketManager.NonSystemMessageReceiveEvent -= AnalyzeAnswerMessage;
    }


    void AnalyzeAnswerMessage(string from ,string to, string answer)
    {
        //analyze only answer messages from ChatScript to Unity.
        if (from == "cs" && to == "unity") {
            // deserialize into WsMessageList
            answerMessageList = JsonUtility.FromJson<AnswerMessageList>(answer);
        
            // get message
            answerMessage = answerMessageList.message[0];
       
            VoiceMessageEvent("Tim", answerMessage.soundFile);

            StartCoroutine(AnalyzeActionState(answerMessage.actionType));
        }
    }



    public IEnumerator AnalyzeActionState(string action)
    {
        //Debug.Log("starting to analyze action");
        string currentAction = "";

        if (action.Contains("communicative"))
        {
            //Debug.Log("Action identified as communicative");
            currentAction = "communicative";
        }

        else if (action.Contains("reserved"))
        {
            //Debug.Log("Actio identified as reserved");
            currentAction = "reserved";
        }


        // change the characters animation state if the currentAction send from the app is different from the previous trial
        if (!string.Equals(currentAction, previousAction))
        {
            ActionStateEvent(currentAction);
            previousAction = currentAction;
        }

        yield return null;
    }


}
