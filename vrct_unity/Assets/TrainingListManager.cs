using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

using System;
using System.Collections;
using System.Collections.Generic;


public class TrainingListManager : MonoBehaviour
{

    public string apiHost = "127.0.0.1";
    public string apiPort = "8000";
    private string baseURL;

    private GameObject layoutContainer;

    [SerializeField]
    private GameObject itemTemplatePrefab;

    [Serializable]
    public class ApiUser
    {
        public string token;
        public string user_id;
        public string username;
    }

    public ApiUser User = new ApiUser();

    // from: https://stackoverflow.com/questions/36239705/serialize-and-deserialize-json-and-json-array-in-unity
    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }

    [Serializable]
    public class Training
    {
        public string training;
        public string trainee;
        public string condition;
        public string bot;
        public string bot_variant;
        public string ws_room;
        public string unity_scene;
    }

    // Start is called before the first frame update
    void Start()
    {
        layoutContainer = GameObject.Find("/Canvas/ListContainer/ViewPort/LayoutContainer");

        baseURL = "http://" + apiHost + ":" + apiPort + "/";

        StartCoroutine(Login());
    }

    IEnumerator Login()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", "vrct");
        form.AddField("password", "vrct");

        Debug.Log(form);

        string endpoint = "api/api-token-auth/";

        using (UnityWebRequest www = UnityWebRequest.Post(baseURL + endpoint, form))
        {            
            Debug.Log(www);

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Succesfully logged into backend!");

                // Show results as text
                Debug.Log(www.downloadHandler.text);

                

                User = JsonUtility.FromJson<ApiUser>(www.downloadHandler.text);

                Debug.Log(User.token);

                StartCoroutine(GetCurrentRunningExperiments());

            }
        }

    }

    IEnumerator GetCurrentRunningExperiments() {

        string endpoint = "api/current-trainings-list-unity/";

        using (UnityWebRequest www = UnityWebRequest.Get(baseURL + endpoint))
        {

            www.SetRequestHeader("Authorization", "Token " + User.token);

            yield return www.SendWebRequest();
 
            if(www.result != UnityWebRequest.Result.Success) {
                Debug.Log(www.error);
            }
            else {
                // Show results as text
                Debug.Log(www.downloadHandler.text);

                Training[] trainingList = JsonHelper.FromJson<Training>(www.downloadHandler.text);

                if (trainingList != null)
                {
                    foreach (Training training in trainingList)
                    {
                        GameObject listItem = Instantiate(itemTemplatePrefab) as GameObject;
                        listItem.transform.SetParent(layoutContainer.transform, false);

                        Transform listItemTextTrainingTransform = listItem.transform.Find("Panel/ListItemTextTraining");
                        GameObject listItemTextTraining = listItemTextTrainingTransform.gameObject;
                        Text listItemTextTrainingComponent = listItemTextTraining.GetComponent<Text>();
                        listItemTextTrainingComponent.text = "Training: " + training.training;

                        Transform listItemTextTraineeTransform = listItem.transform.Find("Panel/ListItemTextTrainee");
                        GameObject listItemTextTrainee = listItemTextTraineeTransform.gameObject;
                        Text listItemTextTraineeComponent = listItemTextTrainee.GetComponent<Text>();
                        listItemTextTraineeComponent.text = "Trainee: " + training.trainee;

                        Transform listItemTextConditionTransform = listItem.transform.Find("Panel/ListItemTextCondition");
                        GameObject listItemTextCondition = listItemTextConditionTransform.gameObject;
                        Text listItemTextConditionComponent = listItemTextCondition.GetComponent<Text>();
                        listItemTextConditionComponent.text = "Condition: " + training.condition;

                        Transform listItemTextBotTransform = listItem.transform.Find("Panel/ListItemTextBot");
                        GameObject listItemTextBot = listItemTextBotTransform.gameObject;
                        Text listItemTextBotComponent = listItemTextBot.GetComponent<Text>();
                        listItemTextBotComponent.text = "Bot: " + training.bot;

                        Transform listItemTextBotVariantTransform = listItem.transform.Find("Panel/ListItemTextBotVariant");
                        GameObject listItemTextBotVariant = listItemTextBotVariantTransform.gameObject;
                        Text listItemTextBotVariantComponent = listItemTextBotVariant.GetComponent<Text>();
                        listItemTextBotVariantComponent.text = "Bot-Variant: " + training.bot_variant;

                        // Button to Start
                        Transform listItemButtonTransform = listItem.transform.Find("Panel/ListItemButton");
                        GameObject button = listItemButtonTransform.gameObject;
                        Button btn = button.GetComponent<Button>();
		                btn.onClick.AddListener(() => TaskOnClick(training));
                    }
                }
    
            }
        }
        
    }

    void TaskOnClick(Training training){

        // get TrainingManager
        GameObject trainingManager = GameObject.Find("TrainingManager");

        // save training in currentTraining
        trainingManager.GetComponent<TrainingManager>().currentTraining.training = training.training;
        trainingManager.GetComponent<TrainingManager>().currentTraining.trainee = training.trainee;
        trainingManager.GetComponent<TrainingManager>().currentTraining.condition = training.condition;
        trainingManager.GetComponent<TrainingManager>().currentTraining.bot = training.bot;
        trainingManager.GetComponent<TrainingManager>().currentTraining.bot_variant = training.bot_variant;
        trainingManager.GetComponent<TrainingManager>().currentTraining.ws_room = training.ws_room;
        trainingManager.GetComponent<TrainingManager>().currentTraining.unity_scene = training.unity_scene;

        // load basic waiting scene
        SceneManager.LoadScene("WaitScene");
	}
}
