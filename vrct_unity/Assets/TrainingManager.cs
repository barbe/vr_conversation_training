using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Valve.VR;

public class TrainingManager : MonoBehaviour
{

    // current training classs
    [Serializable]
    public class CurrentTraining
    {
        public string training;
        public string trainee;
        public string condition;
        public string bot;
        public string bot_variant;
        public string ws_room;
        public string unity_scene;
    }

    public CurrentTraining currentTraining = new CurrentTraining();

    public delegate void StatusEvent();
    public static event StatusEvent LevelChangeEvent;

    public State current_state;

    //index of all states representing phases within one trial
    public enum State
    {
        Waiting,  // APP is connected, waiting for start of training
        Training, // training is running

        TrainingStopped, // waiting after training has been stopped

        End,    // waiting for restart
    }

    //executes code dependent on current state
    public string TransitionState()
    {
        switch (current_state)
        {

            case State.Waiting:
                WaitForStart();
                break;
            case State.Training:
                SetupTraining();
                break;

            case State.TrainingStopped:
                TrainingStopped();
                break;

            case State.End:
                EndFunctionality();
                break;
        }
        return string.Empty;

    }

    // Start is called before the first frame update
    void Start()
    {
        WebsocketManager.MessageReceiveEvent += AnalyzeMessage;
    }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void WaitForStart()
    {
        Debug.Log("Waiting for Start of tutorial or simulation...");

    }

    private void SetupTraining()
    {
        Debug.Log("trying to load training scene");

        // load basic training scene
        // ToDo: define prefabs necessary for the choosen training in backend 
        // StartCoroutine(LoadTrainingScene());


       // SteamVR_LoadLevel.Begin(currentTraining.unity_scene);
       StartCoroutine(LoadTrainingScene());
        LevelChangeEvent();


        Debug.Log("...training scene started.");
    }




    private void TrainingStopped()
    {
        Debug.Log("trying to load training stopped scene");

        // load basic training scene
        // ToDo: define prefabs necessary for the choosen training in backend 
        StartCoroutine(LoadTrainingStoppedScene());


       // SteamVR_LoadLevel.Begin("TrainingStoppedScene");
        LevelChangeEvent();

        GameObject controller = GameObject.Find("WebsocketManager");
        WebsocketManager websocketManager = controller.GetComponent<WebsocketManager>();
        websocketManager.SendMessage("unity", "app", "system", "training scene stopped");


    }



    private void EndFunctionality()
    {
        Debug.Log("trying to load end scene");

        // load basic training scene
        // ToDo: define prefabs necessary for the choosen training in backend 
        StartCoroutine(LoadEndScene());

        Debug.Log("...end scene started.");
    }

  
     IEnumerator LoadTrainingScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        // load the scene defined in the condition of the current training
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(currentTraining.unity_scene);
    
        LevelChangeEvent();

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        if (asyncLoad.isDone == true)
        {
            // app waits for this message
         Debug.Log("...finished async loading.");
        GameObject controller = GameObject.Find("WebsocketManager");
        WebsocketManager websocketManager = controller.GetComponent<WebsocketManager>();
        websocketManager.SendMessage("unity", "app", "system", "training scene started");
        }
    }
    IEnumerator LoadTrainingStoppedScene()
    {
        // load end scene
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("TrainingStoppedScene");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        if (asyncLoad.isDone == true)
        {
            // app waits for this message
            GameObject controller = GameObject.Find("WebsocketManager");
            WebsocketManager websocketManager = controller.GetComponent<WebsocketManager>();
            websocketManager.SendMessage("unity", "app", "system", "training scene stopped");
        }
    }

    IEnumerator LoadEndScene()
    {
        // load end scene
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("EndScene");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        if (asyncLoad.isDone == true)
        {
            // finished message must be sent from EndManager!

        }
    }
    void AnalyzeMessage(string from, string to, string type, string text)
    {
        // Debug
        if (type != "heartbeat")
        {
            Debug.Log("AnalyzeMessage from " + from + " to " + to + " received.\ntext: " + text + "[type: " + type + "]");
        }

        // start training
        if (from == "app" && to == "unity")
        {
            if (type == "system")
            {

                if (text == "start training scene")

                {
                    Debug.Log("Try to start training scene...");
                    current_state = State.Training;
                    TransitionState();
                }
                else if (text == "stop training scene")
                {
                    Debug.Log("Try to stop training scene...");
                    current_state = State.TrainingStopped;
                    TransitionState();
                }
                else if (text == "restart training")
                {
                    Debug.Log("Try to restart training scene...");
                    current_state = State.Training;
                    TransitionState();
                }
                else if (text == "finish training")
                {
                    Debug.Log("Try to stop training scene...");
                    current_state = State.End;
                    TransitionState();
                }

            }
        }


    }


}
