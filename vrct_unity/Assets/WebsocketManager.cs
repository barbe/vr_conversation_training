using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NativeWebSocket;


public class WebsocketManager : MonoBehaviour
{

    private GameObject trainingManager;

    public string wsRoom;
    public string wsHost = "127.0.0.1";
    public string wsPort = "8000";
    public string wsAdress;

    [Serializable]
    public class WsMessageList
    {
        public WsMessage[] message;
    }

    [Serializable]
    public class WsMessage
    {
        public string from;
        public string to;
        public string text;
        public string type;

    }

    WebSocket websocket;

    public delegate void MyDelegateEvent(string from, string to, string type, string text);
    public static event MyDelegateEvent MessageReceiveEvent;

    public delegate void NonSystemMessageEvent(string from, string to, string nsmessage);
    public static event NonSystemMessageEvent NonSystemMessageReceiveEvent;

    // Start is called before the first frame update
    async void Start()
    {
        trainingManager = GameObject.Find("TrainingManager");
        wsRoom = trainingManager.GetComponent<TrainingManager>().currentTraining.ws_room;

        wsAdress = "ws://" + wsHost + ":" + wsPort + "/ws/chat/" + wsRoom + "/";

        websocket = new WebSocket(wsAdress);

        websocket.OnOpen += () =>
        {
            Debug.Log("Websocket Connection open!");
        };

        websocket.OnError += (e) =>
        {
            Debug.Log("Websocket Error! " + e);
        };

        websocket.OnClose += (e) =>
        {
            Debug.Log("Websocket Connection closed!");
        };

        websocket.OnMessage += (bytes) =>
        {
            // getting the message as a string
            var rawMessage = System.Text.Encoding.UTF8.GetString(bytes);

            
            // we now must fix the json and add {message: } to it
            var fixedMessage = "{\"message\":" + rawMessage + "}";

            // deserialize into WsMessageList
            var messageList = JsonUtility.FromJson<WsMessageList>(fixedMessage);


            // get message
            WsMessage message = messageList.message[0];

            // Debug
           // Debug.Log("New message from " + message.from + " to " + message.to + " received.\ntext: " + message.text + "[type: " + message.type + "]");

            // check if message contains 'system' or 'heartbeat' to trigger level changes or different training states. 
            
            if (message.type == "system" || message.type == "heartbeat") {
            // send message event
            MessageReceiveEvent(message.from, message.to, message.type, message.text);

            //Else message has to be an answer from ChatScript(NonSystemMessage) which triggers a corresponding animation and audio output.
            } else {

            NonSystemMessageReceiveEvent(message.from, message.to, fixedMessage);
            }
        
        };

        // Keep sending heartbeat messages at every 2.0s
        InvokeRepeating("SendHeartbeatMessage", 0.0f, 2.0f);

        // waiting for messages
        await websocket.Connect();

    }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        #if !UNITY_WEBGL || UNITY_EDITOR
        websocket.DispatchMessageQueue();
        #endif
    }

    async void SendHeartbeatMessage()
    {
        if (websocket.State == WebSocketState.Open)
        {         
            // django channels need a json message
            WsMessage newMessage = new WsMessage();

            newMessage.from = "unity";
            newMessage.to = "app";
            newMessage.text = "heartbeat";
            newMessage.type = "heartbeat";

          //  Debug.Log(JsonUtility.ToJson(newMessage));

            // we need to put the message in a list: that creates a json with {message: [{...}]}
            WsMessageList message = new WsMessageList();
            message.message = new WsMessage[] { newMessage };

         //   Debug.Log(JsonUtility.ToJson(message));



            // Sending message
            await websocket.SendText(JsonUtility.ToJson(message));
        }
    }

    public async void SendMessage(string from, string to, string type, string text)
    {
        if (websocket.State == WebSocketState.Open)
        {         
            // django channels need a json message
            WsMessage newMessage = new WsMessage();

            newMessage.from = from;
            newMessage.to = to;
            newMessage.text = text;
            newMessage.type = type;

            // we need to put the message in a list: that creates a json with {message: [{...}]}
            WsMessageList message = new WsMessageList();
            message.message = new WsMessage[] { newMessage };

            Debug.Log(JsonUtility.ToJson(message));

            // Sending message
            await websocket.SendText(JsonUtility.ToJson(message));
        }
    }

    private async void OnApplicationQuit()
    {
        await websocket.Close();
    }

    public async void CloseWebsocket()
    {
        // cancel heartbeat invoke
        CancelInvoke();

        // close websocket
        await websocket.Close();
    }

}
